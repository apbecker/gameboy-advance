module GameBoyAdvance.Memory.VRAM

open GameBoyAdvance
open GameBoyAdvance.Platform
open GameBoyAdvance.Platform.Util


let init () =
  Memory.ram (BinaryUnit.byte 0x18000)


let maskAddress address =
  let mask =
    if (address &&& 0x10000) <> 0 then
      0x17fff
    else
      0x0ffff

  address &&& mask


let read size address memory =
  let address = maskAddress address

  match size with
  | Size.Byte -> memory |> Memory.getUInt8  address |> int32
  | Size.Half -> memory |> Memory.getUInt16 address |> int32
  | Size.Word -> memory |> Memory.getUInt32 address |> int32


let write size address data memory =
  let address = maskAddress address

  match size with
  | Size.Byte
  | Size.Half -> memory |> Memory.setUInt16 address (uint16 data)
  | Size.Word -> memory |> Memory.setUInt32 address (uint32 data)
