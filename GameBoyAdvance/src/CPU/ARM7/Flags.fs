namespace GameBoyAdvance.Processors.ARM7

type Flags =
  { mutable n: int
    mutable z: int
    mutable c: int
    mutable v: int
    mutable r: int
    mutable i: int
    mutable f: int
    mutable t: int
    mutable m: int }

module Flags =

  let init () =
    { n = 0
      z = 0
      c = 0
      v = 0
      r = 0
      i = 0
      f = 0
      t = 0
      m = 0 }


  let load (value: int) flags =
    flags.n <- (value >>> 31) &&& 1
    flags.z <- (value >>> 30) &&& 1
    flags.c <- (value >>> 29) &&& 1
    flags.v <- (value >>> 28) &&& 1
    flags.r <- (value >>>  8) &&& 0xfffff
    flags.i <- (value >>>  7) &&& 1
    flags.f <- (value >>>  6) &&& 1
    flags.t <- (value >>>  5) &&& 1
    flags.m <- (value >>>  0) &&& 31


  let save flags =
    (flags.n <<< 31) |||
    (flags.z <<< 30) |||
    (flags.c <<< 29) |||
    (flags.v <<< 28) |||
    (flags.r <<<  8) |||
    (flags.i <<<  7) |||
    (flags.f <<<  6) |||
    (flags.t <<<  5) |||
    (flags.m <<<  0)


  let copy source target =
    load (save source) target
