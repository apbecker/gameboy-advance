namespace GameBoyAdvance.Processors.ARM7

type Pipeline =
  { mutable execute: int
    mutable decode: int
    mutable fetch: int
    mutable refresh: bool }

module Pipeline =

  let init () =
    { execute = 0
      decode = 0
      fetch = 0
      refresh = false }
