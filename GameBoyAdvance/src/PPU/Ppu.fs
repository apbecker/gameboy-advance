namespace GameBoyAdvance.PPU

open GameBoyAdvance
open GameBoyAdvance.CPU
open GameBoyAdvance.Memory
open GameBoyAdvance.PPU.Backgrounds
open GameBoyAdvance.PPU.Sprites
open GameBoyAdvance.PPU.Windows
open GameBoyAdvance.Platform.Hosting

type Ppu =
  { blend: Blend
    bkg: BackgroundController
    spr: SpriteController
    window: WindowController
    registers: byte[]

    mutable forcedBlank: bool
    mutable hblankIntervalFree: bool
    mutable hblank: bool
    mutable vblank: bool
    mutable vmatch: bool
    mutable hblankIrq: bool
    mutable vblankIrq: bool
    mutable vmatchIrq: bool
    mutable vcheck: int
    mutable hclock: int
    mutable vclock: int
    mutable cycles: int }

module Ppu =

  let colorLut =
    let smooth n = n ||| (n >>> 5)

    [|
      for i in 0 .. 32767 do
        let r = smooth ((i <<< 3) &&& 0xf8)
        let g = smooth ((i >>> 2) &&& 0xf8)
        let b = smooth ((i >>> 7) &&& 0xf8)

        yield (r <<< 16) ||| (g <<< 8) ||| (b <<< 0)
    |]


  let init () =
    let window = WindowController.init ()

    { window = window
      blend = Blend.init ()
      bkg = BackgroundController.init ()
      spr = SpriteController.init (WindowController.getSpriteWindow window)
      registers = Array.zeroCreate 256

      forcedBlank = false
      hblankIntervalFree = false
      hblank = false
      vblank = false
      vmatch = false
      hblankIrq = false
      vblankIrq = false
      vmatchIrq = false
      vcheck = 0
      hclock = 0
      vclock = 0
      cycles = 0 }


  let blendColors color1 eva color2 evb =
    let r1 = (color1 >>>  0) &&& 31
    let g1 = (color1 >>>  5) &&& 31
    let b1 = (color1 >>> 10) &&& 31
    let r2 = (color2 >>>  0) &&& 31
    let g2 = (color2 >>>  5) &&& 31
    let b2 = (color2 >>> 10) &&& 31

    let mutable r = ((r1 * eva) + (r2 * evb)) >>> 4
    let mutable g = ((g1 * eva) + (g2 * evb)) >>> 4
    let mutable b = ((b1 * eva) + (b2 * evb)) >>> 4

    if r > 31 then r <- 31
    if g > 31 then g <- 31
    if b > 31 then b <- 31

    (r <<< 0) ||| (g <<< 5) ||| (b <<< 10)


  let blend semiTransparent mask1 mask2 color1 color2 ppu =
    let blend1 = (ppu.blend.target1 &&& mask1) <> 0
    let blend2 = (ppu.blend.target2 &&& mask2) <> 0

    if semiTransparent && blend2 then
      blendColors color1 ppu.blend.eva color2 ppu.blend.evb
    elif ppu.blend.type' = 1 && blend1 && blend2 then
      blendColors color1 ppu.blend.eva color2 ppu.blend.evb
    elif ppu.blend.type' = 2 && blend1 then
      blendColors color1 (16 - ppu.blend.evy) 0x7fff ppu.blend.evy
    elif ppu.blend.type' = 3 && blend1 then
      blendColors color1 (16 - ppu.blend.evy) 0x0000 ppu.blend.evy
    else
      color1


  let getPixelSources x windowFlags ppu =
    let target1 = ref Backdrop.init
    let target2 = ref Backdrop.init

    for p = 3 downto 0 do
      ppu.bkg |> BackgroundController.getPixel windowFlags x p target1 target2
      ppu.spr |> SpriteController.getPixel windowFlags x p target1 target2

    (!target1, !target2)


  let blendLayers reader video ppu =
    for x in 0 .. 239 do
      let y = int ppu.vclock
      let windowFlags = ppu.window |> WindowController.getFlags x y

      let (target1, target2) = ppu |> getPixelSources x windowFlags

      let color1 = reader Size.Half (target1.GetColorAddress(x))
      let color2 = reader Size.Half (target2.GetColorAddress(x))

      let color =
        if (windowFlags &&& WindowFlags.Clr) <> WindowFlags.None then
          ppu |> blend (target1.IsSemiTransparent x) (target1.GetMask()) (target2.GetMask()) color1 color2
        else
          color1

      VideoSink.run video x y (colorLut.[color &&& 0x7fff])


  let enterHBlank irq hblank ppu =
    ppu.hblank <- true

    ppu.bkg |> BackgroundController.clockAffine

    if ppu.hblankIrq then
      irq Interrupt.HBlank

    if ppu.vclock < 160 then
      hblank ()

      (*
      if (cpu.dma[0].Enabled && cpu.dma[0].Type == Dma.HBlank) cpu.dma[0].Pending = true
      if (cpu.dma[1].Enabled && cpu.dma[1].Type == Dma.HBlank) cpu.dma[1].Pending = true
      if (cpu.dma[2].Enabled && cpu.dma[2].Type == Dma.HBlank) cpu.dma[2].Pending = true
      if (cpu.dma[3].Enabled && cpu.dma[3].Type == Dma.HBlank) cpu.dma[3].Pending = true
      *)


  let enterVBlank irq vblank ppu =
    ppu.vblank <- true

    if ppu.vblankIrq then
      irq Interrupt.VBlank

    vblank ()

    (*
    if (cpu.dma[0].Enabled && cpu.dma[0].Type == Dma.VBlank) cpu.dma[0].Pending = true
    if (cpu.dma[1].Enabled && cpu.dma[1].Type == Dma.VBlank) cpu.dma[1].Pending = true
    if (cpu.dma[2].Enabled && cpu.dma[2].Type == Dma.VBlank) cpu.dma[2].Pending = true
    if (cpu.dma[3].Enabled && cpu.dma[3].Type == Dma.VBlank) cpu.dma[3].Pending = true
    *)


  let leaveHBlank ppu =
    ppu.hblank <- false


  let leaveVBlank ppu =
    ppu.vblank <- false

    ppu.bkg |> BackgroundController.resetAffine


  let updateVCheck irq ppu =
    ppu.vmatch <- (ppu.vclock = ppu.vcheck)

    if ppu.vmatch && ppu.vmatchIrq then
      irq Interrupt.VCheck


  let updateVClock irq vblank ppu =
    ppu.vclock <- ppu.vclock + 1

    if ppu.vclock = 160 then
      ppu |> enterVBlank irq vblank

    if ppu.vclock = 228 then
      ppu |> leaveVBlank
      ppu.vclock <- 0

    ppu |> updateVCheck irq


  let updateHClock irq hblank vblank ppu =
    ppu.hclock <- ppu.hclock + 1

    if ppu.hclock = 240 then
      ppu |> enterHBlank irq hblank


    if ppu.hclock = 308 then
      ppu |> leaveHBlank
      ppu.hclock <- 0
      ppu |> updateVClock irq vblank


  let private readReg ppu = IOReader (fun address ->
    ppu.registers.[address &&& 0xff]
  )


  let private read004 ppu = IOReader (fun _ ->
    let mutable data = ppu.registers.[0x04]

    if ppu.vblank then data <- data ||| 0x01uy
    if ppu.hblank then data <- data ||| 0x02uy
    if ppu.vmatch then data <- data ||| 0x04uy

    data
  )


  let private read005 ppu = IOReader (fun _ ->
    byte ppu.vcheck
  )


  let private read006 ppu = IOReader (fun _ ->
    byte (ppu.vclock >>> 0)
  )


  let private read007 ppu = IOReader (fun _ ->
    byte (ppu.vclock >>> 8)
  )


  let private write000 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x00] <- data
    let data = int data
    ppu.bkg.mode <- (data &&& 0x07)
    ppu.bkg.baseAddress <- if (data &&& 0x10) <> 0 then 0xa000 else 0x0000
    ppu.hblankIntervalFree <- (data &&& 0x20) <> 0
    ppu.spr.mapping <- (data &&& 0x40) <> 0
    ppu.forcedBlank <- (data &&& 0x80) <> 0
  )


  let private write001 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x01] <- data

    ppu.bkg |> BackgroundController.setEnabled
      ((data &&& 0x01uy) <> 0uy)
      ((data &&& 0x02uy) <> 0uy)
      ((data &&& 0x04uy) <> 0uy)
      ((data &&& 0x08uy) <> 0uy)

    ppu.spr.enabled <- (data &&& 0x10uy) <> 0uy

    ppu.window |> WindowController.setEnabled
      ((data &&& 0x20uy) <> 0uy)
      ((data &&& 0x40uy) <> 0uy)
      ((data &&& 0x80uy) <> 0uy)
  )


  let private write002 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x02] <- data &&& 0x01uy
  )


  let private write003 ppu = IOWriter (fun _ _ ->
    ppu.registers.[0x03] <- 0uy
  )


  let private write004 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x04] <- data &&& 0x38uy
    ppu.vblankIrq <- (data &&& 0x08uy) <> 0uy
    ppu.hblankIrq <- (data &&& 0x10uy) <> 0uy
    ppu.vmatchIrq <- (data &&& 0x20uy) <> 0uy
  )


  let private write005 ppu = IOWriter (fun _ data ->
    ppu.vcheck <- int data
  )


  let private write04C ppu = IOWriter (fun _ data ->
    let data = int data
    ppu.bkg.mosaicH <- (data >>> 0) &&& 15
    ppu.bkg.mosaicV <- (data >>> 4) &&& 15
  )


  let private write04D ppu = IOWriter (fun _ data ->
    let data = int data
    ppu.spr.mosaicH <- (data >>> 0) &&& 15
    ppu.spr.mosaicV <- (data >>> 4) &&& 15
  )


  let private write050 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x50] <- data
    let data = int data
    ppu.blend.target1 <- (data >>> 0) &&& 63
    ppu.blend.type'   <- (data >>> 6) &&& 3
  )


  let private write051 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x51] <- data &&& 0x3fuy
    let data = int data
    ppu.blend.target2 <- (data >>> 0) &&& 63
  )


  let private write052 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x52] <- data &&& 0x1fuy
    let data = int data
    ppu.blend.eva <- max 16 (data &&& 0x1f)
  )


  let private write053 ppu = IOWriter (fun _ data ->
    ppu.registers.[0x53] <- data &&& 0x1fuy
    let data = int data
    ppu.blend.evb <- max 16 (data &&& 0x1f)
  )


  let private write054 ppu = IOWriter (fun _ data ->
    let data = int data
    ppu.blend.evy <- max 16 (data &&& 0x1f)
  )


  let wireUp mmio ppu =
    mmio |> MMIO.rw 0x000 (readReg ppu) (write000 ppu)
    mmio |> MMIO.rw 0x001 (readReg ppu) (write001 ppu)
    mmio |> MMIO.rw 0x002 (readReg ppu) (write002 ppu)
    mmio |> MMIO.rw 0x003 (readReg ppu) (write003 ppu)
    mmio |> MMIO.rw 0x004 (read004 ppu) (write004 ppu)
    mmio |> MMIO.rw 0x005 (read005 ppu) (write005 ppu)
    // vertical counter
    mmio |> MMIO.ro 0x006 (read006 ppu)
    mmio |> MMIO.ro 0x007 (read007 ppu)
    // window feature
    mmio |> MMIO.wo 0x040 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x041 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x042 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x043 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x044 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x045 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x046 (WindowController.write ppu.window)
    mmio |> MMIO.wo 0x047 (WindowController.write ppu.window)
    mmio |> MMIO.rw 0x048 (WindowController.read ppu.window) (WindowController.write ppu.window)
    mmio |> MMIO.rw 0x049 (WindowController.read ppu.window) (WindowController.write ppu.window)
    mmio |> MMIO.rw 0x04a (WindowController.read ppu.window) (WindowController.write ppu.window)
    mmio |> MMIO.rw 0x04b (WindowController.read ppu.window) (WindowController.write ppu.window)
    // mosaic
    mmio |> MMIO.rw 0x04c (readReg ppu) (write04C ppu)
    mmio |> MMIO.rw 0x04d (readReg ppu) (write04D ppu)
    // 04e - 04f
    mmio |> MMIO.rw 0x050 (readReg ppu) (write050 ppu)
    mmio |> MMIO.rw 0x051 (readReg ppu) (write051 ppu)
    mmio |> MMIO.rw 0x052 (readReg ppu) (write052 ppu)
    mmio |> MMIO.rw 0x053 (readReg ppu) (write053 ppu)
    mmio |> MMIO.rw 0x054 (readReg ppu) (write054 ppu)
    // 054 - 05f


  let renderScanline reader video ppu =
    if ppu.forcedBlank then
      for x = 0 to 239 do
        VideoSink.run video x ppu.vclock colorLut.[0x7fff]
    else
      ppu.spr |> SpriteController.render reader ppu.vclock
      ppu.bkg |> BackgroundController.render reader ppu.vclock

      ppu |> blendLayers reader video


  let tick irq hblank vblank reader video ppu =
    ppu |> updateHClock irq hblank vblank

    if ppu.hclock = 240 && ppu.vclock < 160 then
      ppu |> renderScanline reader video


  let clock irq hblank vblank reader amount video ppu =
    ppu.cycles <- ppu.cycles + amount
    while ppu.cycles >= 4 do
      ppu.cycles <- ppu.cycles - 4
      ppu |> tick irq hblank vblank reader video
