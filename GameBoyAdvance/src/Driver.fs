namespace GameBoyAdvance

open GameBoyAdvance.APU
open GameBoyAdvance.CPU
open GameBoyAdvance.Memory
open GameBoyAdvance.PPU
open GameBoyAdvance.Platform.Hosting
open GameBoyAdvance.Platform.Util

module Driver =

  let private create = DriverFactory (fun binary ->
    let bios = BIOS.init (FileHelper.readAllBytes "gba/bios.rom")
    let gamePak = GamePak.init binary

    let memory = MemoryMap.init bios gamePak

    let dma = DmaController.init ()
    let apu = Apu.init ()
    let timer = TimerController.init ()
    let cpu = Cpu.init ()
    let pad = Pad.init ()
    let ppu = Ppu.init ()

    let reader size address =
      memory |> MemoryMap.timedRead cpu (Cpu.addClock cpu) size address

    let writer size address data =
      memory |> MemoryMap.timedWrite (Cpu.addClock cpu) size address data

    apu |> Apu.wireUp memory.mmio
    dma |> DmaController.wireUp memory.mmio
    gamePak |> GamePak.wireUp memory.mmio
    pad |> Pad.wireUp memory.mmio
    ppu |> Ppu.wireUp memory.mmio
    timer |> TimerController.wireUp memory.mmio

    let irq e = Cpu.irq cpu e
    let runDMA () = DmaController.transfer reader writer irq dma
    let hblank () = DmaController.hBlank dma
    let vblank () = DmaController.vBlank dma

    let requestMoreData1 () =
      let dma = dma.channels.[1]
      if Dma.getEnabled dma && Dma.getType dma = DmaType.Special then
        dma.Pending <- true

    let requestMoreData2 () =
      let dma = dma.channels.[2]
      if Dma.getEnabled dma && Dma.getType dma = DmaType.Special then
        dma.Pending <- true

    let elapsed n =
      if apu.soundA.timer = n then apu.soundA |> ChannelPCM.clock requestMoreData1
      if apu.soundB.timer = n then apu.soundB |> ChannelPCM.clock requestMoreData2

    let mutable cycles = 0

    let runForOneFrame audio video =
      let cyclesPerSecond = 16777216

      Pad.frame pad

      while cycles < cyclesPerSecond do
        let adding = Cpu.update reader writer runDMA cpu

        apu |> Apu.clock adding audio
        ppu |> Ppu.clock irq hblank vblank reader adding video
        timer |> TimerController.clock irq elapsed adding

        cycles <- cycles + (adding * 60)

      cycles <- cycles - cyclesPerSecond
    in
    Driver(runForOneFrame)
  )


  let definition =
    { name = "Game Boy Advance"
      extensions = [ ".gba" ]
      audio =
        { backend = "sdl2"
          channels = 2
          sampleRate = 48000 }
      video =
        { backend = "sdl2"
          width = 240
          height = 160 }
      factory = create }
