namespace GameBoyAdvance.PPU.Sprites

open GameBoyAdvance
open GameBoyAdvance.PPU
open GameBoyAdvance.PPU.Windows

type SpriteController =
  { window: SpriteWindow
    pixels: SpritePixel[]

    mutable mosaicH: int
    mutable mosaicV: int
    mutable enabled: bool
    mutable mapping: bool }

module SpriteController =

  let [<Literal>] COUNT = 128


  let init window =
    { window = window
      pixels = Array.zeroCreate 240
      
      mosaicH = 0
      mosaicV = 0

      enabled = false
      mapping = false }


  let readOram16 reader (address: int) =
    reader Size.Half (Constants.OramBase + address * 2)


  let readVram8 reader (address: int) =
    reader Size.Byte (Constants.VramBase + address)


  let readVram16 reader (address: int) =
    reader Size.Half (Constants.VramBase + address * 2)


  let outputPixel (sprite: Sprite) (x: int) (address: int) ctrl =
    if sprite.attr0.mode = SpriteMode.Window then
      SpriteWindow.pixel x ctrl.window
    else
      if ctrl.pixels.[x].priority >= sprite.attr2.priority then
        ctrl.pixels.[x].priority <- sprite.attr2.priority
        ctrl.pixels.[x].opaque <- true
        ctrl.pixels.[x].colorAddress <- Constants.PramBase + 0x200 + (address * 2)
        ctrl.pixels.[x].semiTransparent <- sprite.attr0.mode = SpriteMode.SemiTransparent


  let outputPixel8Bpp reader (sprite: Sprite) (x: int) (address: int) ctrl =
    let colorIndex = readVram8 reader (0x10000 + address)
    if colorIndex <> 0 then
      ctrl |> outputPixel sprite x colorIndex


  let outputPixel4Bpp reader (sprite: Sprite) (x: int) (address: int) (tx: int) ctrl =
    let colorIndex = readVram8 reader (0x10000 + address)

    let colorIndex =
      if (tx &&& 1) = 0 then
        (colorIndex >>> 0) &&& 15
      else
        (colorIndex >>> 4) &&& 15

    if colorIndex <> 0 then
      ctrl |> outputPixel sprite x ((sprite.attr2.palette <<< 4) + colorIndex)


  let renderAffine reader (sprite: Sprite) (line: int) (scale: int) ctrl =
    let dx  = readOram16 reader ((sprite.attr1.param <<< 4) ||| 0x3) |> int16 |> int32
    let dmx = readOram16 reader ((sprite.attr1.param <<< 4) ||| 0x7) |> int16 |> int32
    let dy  = readOram16 reader ((sprite.attr1.param <<< 4) ||| 0xb) |> int16 |> int32
    let dmy = readOram16 reader ((sprite.attr1.param <<< 4) ||| 0xf) |> int16 |> int32

    let pitch =
      if ctrl.mapping then
        (sprite.w / 8) * scale // 1 dimensional
      else
        0x20 // 2 dimensional

    let cx = (sprite.clippingW / 2)
    let cy = line - (sprite.clippingH / 2)
    let mutable rx = (cy * dmx) - (cx * dx) + (sprite.w <<< 7)
    let mutable ry = (cy * dmy) - (cx * dy) + (sprite.h <<< 7)

    for i in 0 .. sprite.clippingW - 1 do
      let tx = rx >>> 8
      let ty = ry >>> 8

      rx <- rx + dx
      ry <- ry + dy

      if (tx >= 0 && tx < sprite.w && ty >= 0 && ty < sprite.h) then
        let x = (sprite.attr1.x + i) &&& 0x1ff
        if x < 240 then
          let address =
            (sprite.attr2.tile + ((ty / 8) * pitch) + ((tx / 8) * scale)) * 32 +
              ((ty &&& 7) * 4 * scale) +
              ((tx &&& 7) / (2 / scale))

          if sprite.attr0.depth then
            ctrl |> outputPixel8Bpp reader sprite x address
          else
            ctrl |> outputPixel4Bpp reader sprite x address tx


  let renderLinear reader (sprite: Sprite) (line: int) (scale: int) ctrl =
    let line =
      if sprite.attr1.yFlip then
        line ^^^ (sprite.h - 1)
      else
        line

    let mutable baseSprite =
      if ctrl.mapping then
        sprite.attr2.tile + ((line / 8) * (sprite.w / 8)) * scale // 1 dimensional
      else
        sprite.attr2.tile + ((line / 8) * 0x20) // 2 dimensional

    let mutable baseInc = scale

    if sprite.attr1.xFlip then
      baseSprite <- baseSprite + (((sprite.w / 8) - 1) * scale)
      baseSprite <- baseSprite + (((sprite.w / 8) - 1) * scale)
      baseInc <- (-baseInc)

    for i in 0 .. sprite.w - 1 do
      let n = (sprite.attr1.x + i) &&& 0x1ff

      if n < 240 then
        let mutable tx = i &&& 7

        if sprite.attr1.xFlip then
          tx <- tx ^^^ 7

        if sprite.attr0.depth then
          let address = (baseSprite <<< 5) + ((line &&& 7) <<< 3) + (tx >>> 0)
          ctrl |> outputPixel8Bpp reader sprite n address
        else
          let address = (baseSprite <<< 5) + ((line &&& 7) <<< 2) + (tx >>> 1)
          ctrl |> outputPixel4Bpp reader sprite n address tx

      if (i &&& 7) = 7 then baseSprite <- baseSprite + baseInc


  let renderSprite reader y sprite ctrl =
    let line = (y - sprite.attr0.y) &&& 0xff
    if line < sprite.clippingH then
      let scale = if sprite.attr0.depth then 2 else 1

      if sprite.attr0.type' = SpriteType.Linear then
        ctrl |> renderLinear reader sprite line scale
      else
        ctrl |> renderAffine reader sprite line scale


  let render reader y ctrl =
    SpriteWindow.pixelReset ctrl.window

    for x in 0 .. 239 do
      ctrl.pixels.[x].opaque <- false
      ctrl.pixels.[x].semiTransparent <- false
      ctrl.pixels.[x].colorAddress <- Constants.PramBase
      ctrl.pixels.[x].priority <- 5

    if ctrl.enabled then
      for i in COUNT - 1 .. 0 do
        Sprite.fromAttributes
          (readOram16 reader ((i * 4) + 0))
          (readOram16 reader ((i * 4) + 1))
          (readOram16 reader ((i * 4) + 2))
          (fun sprite -> renderSprite reader y sprite ctrl)


  let pixelSource ctrl =
    { new IPixelSource with
        member __.GetColorAddress (x: int) = ctrl.pixels.[x].colorAddress
        member __.GetPriority (x: int) = ctrl.pixels.[x].priority
        member __.GetMask () = 0x10
        member __.IsSemiTransparent (x: int) = ctrl.pixels.[x].semiTransparent }


  let getPixel (windowFlags: WindowFlags) x priority (target1: IPixelSource ref) (target2: IPixelSource ref) ctrl =
    let pixel = ctrl.pixels.[x]
    if ctrl.enabled && pixel.priority = priority && pixel.opaque && (windowFlags &&& WindowFlags.Obj) <> WindowFlags.None then
      target2 := !target1
      target1 := pixelSource ctrl
