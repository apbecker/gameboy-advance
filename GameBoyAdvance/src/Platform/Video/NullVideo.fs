module GameBoyAdvance.Platform.Video.NullVideo

open GameBoyAdvance.Platform.Hosting


let create (_definition: VideoDefinition) =
  ()


let destroy () =
  ()


let sink (_x: int) (_y: int) (_color: int) =
  ()


let render () =
  Continue
