namespace GameBoyAdvance.PPU

type Blend =
  { mutable eva: int
    mutable evb: int
    mutable evy: int
    mutable target1: int
    mutable target2: int
    mutable type': int }

module Blend =

  let init () =
    { eva = 0
      evb = 0
      evy = 0
      target1 = 0
      target2 = 0
      type' = 0 }
