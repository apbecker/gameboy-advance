namespace GameBoyAdvance.Processors.ARM7

type Register =
  { mutable value: int }

module Register =

  let init () =
    { value = 0 }
