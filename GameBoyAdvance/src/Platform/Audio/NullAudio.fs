module GameBoyAdvance.Platform.Audio.NullAudio

open GameBoyAdvance.Platform.Hosting


let create (_definition: AudioDefinition) =
  ()


let destroy () =
  ()


let sink (_sample: int) =
  ()


let render () =
  ()
