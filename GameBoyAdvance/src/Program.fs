module GameBoyAdvance.Program

open System.IO
open GameBoyAdvance.Platform
open GameBoyAdvance.Platform.Audio
open GameBoyAdvance.Platform.Hosting
open GameBoyAdvance.Platform.Video


let private runGame fileName =
  let definition = GameBoyAdvance.Driver.definition
  let binary = File.ReadAllBytes fileName

  let (DriverFactory(factory)) = definition.factory
  let driver = factory binary
  let audio = AudioBackend.create definition.audio
  let video = VideoBackend.create definition.video

  DriverHost.main driver audio video


[<EntryPoint>]
let main args =
  args
    |> Array.tryItem 0
    |> Option.map runGame
    |> Option.defaultValue ()
  0
