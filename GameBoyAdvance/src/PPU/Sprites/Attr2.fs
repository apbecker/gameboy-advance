namespace GameBoyAdvance.PPU.Sprites

type Attr2 =
  { tile: int
    priority: int
    palette: int }

module Attr2 =

  let fromPrimitive value =
    { tile     = (value &&& 0x03ff) >>> 0
      priority = (value &&& 0x0c00) >>> 10
      palette  = (value &&& 0xf000) >>> 12 }
