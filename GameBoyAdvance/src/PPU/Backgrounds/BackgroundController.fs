namespace GameBoyAdvance.PPU.Backgrounds

open GameBoyAdvance.PPU.Windows

type BackgroundController =
  { bg0: LinearBackground
    bg1: LinearBackground
    bg2: AffineBackground
    bg3: AffineBackground

    mutable baseAddress: int
    mutable mode: int
    mutable mosaicH: int
    mutable mosaicV: int }

module BackgroundController =

  let init () =
    { bg0 = LinearBackground.init 0
      bg1 = LinearBackground.init 1
      bg2 = AffineBackground.init 2
      bg3 = AffineBackground.init 3
      baseAddress = 0
      mode = 0
      mosaicH = 0
      mosaicV = 0 }


  let wireUp mmio ctrl =
    mmio |> LinearBackground.wireUp ctrl.bg0
    mmio |> LinearBackground.wireUp ctrl.bg1
    mmio |> AffineBackground.wireUp ctrl.bg2
    mmio |> AffineBackground.wireUp ctrl.bg3


  let clockAffine ctrl =
    AffineBackground.clockAffine ctrl.bg2
    AffineBackground.clockAffine ctrl.bg3


  let resetAffine ctrl =
    AffineBackground.resetAffine ctrl.bg2
    AffineBackground.resetAffine ctrl.bg3


  let setEnabled b0 b1 b2 b3 ctrl =
    ctrl.bg0.enable <- b0
    ctrl.bg1.enable <- b1
    ctrl.bg2.linear.enable <- b2
    ctrl.bg3.linear.enable <- b3


  let render reader vclock ctrl =
    match ctrl.mode with
    | 0 ->
      LinearBackground.renderLinear reader ctrl.bg0 vclock
      LinearBackground.renderLinear reader ctrl.bg1 vclock
      LinearBackground.renderLinear reader ctrl.bg2.linear vclock
      LinearBackground.renderLinear reader ctrl.bg3.linear vclock

    | 1 ->
      LinearBackground.renderLinear reader ctrl.bg0 vclock
      LinearBackground.renderLinear reader ctrl.bg1 vclock
      AffineBackground.renderAffine reader ctrl.bg2

    | 2 ->
      AffineBackground.renderAffine reader ctrl.bg2
      AffineBackground.renderAffine reader ctrl.bg3

    | 3 -> AffineBackground.renderBitmap3 ctrl.bg2
    | 4 -> AffineBackground.renderBitmap4 reader ctrl.bg2 ctrl.baseAddress
    | 5 -> AffineBackground.renderBitmap5 ctrl.bg2 ctrl.baseAddress

    | n -> failwithf "Unknown video mode '%d'." n


  let getPixel windowFlags x priority target1 target2 ctrl =
    if (windowFlags &&& WindowFlags.Bg3) <> WindowFlags.None then
      LinearBackground.getPixel ctrl.bg3.linear x priority target1 target2

    if (windowFlags &&& WindowFlags.Bg2) <> WindowFlags.None then
      LinearBackground.getPixel ctrl.bg2.linear x priority target1 target2

    if (windowFlags &&& WindowFlags.Bg1) <> WindowFlags.None then
      LinearBackground.getPixel ctrl.bg1 x priority target1 target2

    if (windowFlags &&& WindowFlags.Bg0) <> WindowFlags.None then
      LinearBackground.getPixel ctrl.bg0 x priority target1 target2
