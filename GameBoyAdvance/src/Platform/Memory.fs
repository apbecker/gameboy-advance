namespace GameBoyAdvance.Platform

[<Struct>]
type Memory =
  | Ram of Ram:(byte array * int)
  | Rom of Rom:(byte array * int)

module Memory =

  open GameBoyAdvance.Platform.Util


  let rom buffer =
    Rom (buffer, buffer.Length - 1)


  let ram capacity =
    let buffer =
      BinaryUnit.byteSize capacity
        |> MathHelper.nextPowerOfTwo
        |> Array.zeroCreate

    Ram (buffer, buffer.Length - 1)


  // UInt8


  let getUInt8 address memory =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask]
    | Rom (buffer, mask) -> buffer.[address &&& mask]


  let setUInt8 address data memory =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask] <- data
    | Rom _ ->
      ()


  // UInt16


  let getUInt16 address memory =
    let address = address &&& (~~~1) in
    let a = uint16 <| getUInt8 (address + 0) memory in
    let b = uint16 <| getUInt8 (address + 1) memory in

    (b <<< 8) ||| a


  let setUInt16 address (data: uint16) memory =
    let address = address &&& (~~~1) in
    let a = uint8 (data >>> (8 * 0)) in
    let b = uint8 (data >>> (8 * 1)) in

    memory |> setUInt8 (address + 0) a
    memory |> setUInt8 (address + 1) b


  // UInt32


  let getUInt32 address memory =
    let address = address &&& (~~~3) in
    let a = uint32 <| getUInt8 (address + 0) memory in
    let b = uint32 <| getUInt8 (address + 1) memory in
    let c = uint32 <| getUInt8 (address + 2) memory in
    let d = uint32 <| getUInt8 (address + 3) memory in

    (d <<< 24) ||| (c <<< 16) ||| (b <<< 8) ||| a


  let setUInt32 address (data: uint32) memory =
    let address = address &&& (~~~3) in
    let a = uint8 (data >>> (8 * 0)) in
    let b = uint8 (data >>> (8 * 1)) in
    let c = uint8 (data >>> (8 * 2)) in
    let d = uint8 (data >>> (8 * 3)) in

    memory |> setUInt8 (address + 0) a
    memory |> setUInt8 (address + 1) b
    memory |> setUInt8 (address + 2) c
    memory |> setUInt8 (address + 3) d
