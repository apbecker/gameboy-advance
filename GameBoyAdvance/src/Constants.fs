module GameBoyAdvance.Constants

let [<Literal>] PramBase = 0x5000000
let [<Literal>] VramBase = 0x6000000
let [<Literal>] OramBase = 0x7000000
