namespace GameBoyAdvance.PPU.Backgrounds

type AffineBackground =
  { linear: LinearBackground
    mutable pa: int
    mutable pb: int
    mutable pc: int
    mutable pd: int
    mutable rx: int
    mutable ry: int
    mutable rxRegister: int
    mutable ryRegister: int }

module AffineBackground =

  let init index =
    { linear = LinearBackground.init index
      pa = 0
      pb = 0
      pc = 0
      pd = 0
      rx = 0
      ry = 0
      rxRegister = 0
      ryRegister = 0 }


  let clockAffine this =
    this.rx <- this.rx + (int32 (int16 this.pb))
    this.ry <- this.ry + (int32 (int16 this.pd))


  let resetAffine this =
    this.rx <- this.rxRegister
    this.ry <- this.ryRegister


  let renderAffineInternal this w h wrap kernel =
    if this.linear.enable then
      let rx = this.rx
      let ry = this.ry

      for i in 0 .. 239 do
        let ax = rx >>> 8
        let ay = ry >>> 8

        this.rx <- this.rx + (int32 (int16 this.pa))
        this.ry <- this.ry + (int32 (int16 this.pc))

        let inBounds = ax >= 0 && ax < w && ay >= 0 && ay < h
        if inBounds || wrap then
          kernel i ax ay


  let renderBitmap3 bg =
    let baseAddress = 0x0000

    renderAffineInternal bg 240 160 false (fun i ax ay ->
      LinearBackground.outputPixel15Bpp bg.linear i (baseAddress + (ay * 240) + ax)
    )


  let renderBitmap4 reader bg baseAddress =
    renderAffineInternal bg 240 160 false (fun i ax ay ->
      LinearBackground.outputPixel8Bpp reader bg.linear i (baseAddress + (ay * 240) + ax)
    )


  let renderBitmap5 bg baseAddress =
    renderAffineInternal bg 160 128 false (fun i ax ay ->
      LinearBackground.outputPixel15Bpp bg.linear i (baseAddress + (ay * 160) + ax)
    )


  let renderAffine reader bg =
    let size = 1 <<< (bg.linear.screenSize + 7)

    renderAffineInternal bg size size bg.linear.wrap (fun i ax ay ->
      let ix = ax / 8
      let iy = ay / 8
      let indexAddress = bg.linear.indexBase + (iy * (size / 8)) + ix
      let index = LinearBackground.readVram8 reader indexAddress

      let cx = ax &&& 7
      let cy = ay &&& 7
      let colorAddress = bg.linear.colorBase + (index * 64) + (cy * 8) + cx
      let color = LinearBackground.readVram8 reader colorAddress

      LinearBackground.outputPixel bg.linear i 0 color
    )


  let writePA this address (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    this.pa <- this.pa &&& ~~~(0xff <<< shift)
    this.pa <- this.pa |||    (data <<< shift)


  let writePB this address (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    this.pb <- this.pb &&& ~~~(0xff <<< shift)
    this.pb <- this.pb |||    (data <<< shift)


  let writePC this address (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    this.pc <- this.pc &&& ~~~(0xff <<< shift)
    this.pc <- this.pc |||    (data <<< shift)


  let writePD this address (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    this.pd <- this.pd &&& ~~~(0xff <<< shift)
    this.pd <- this.pd |||    (data <<< shift)


  let writeRX this address (data: byte) =
    let index = address &&& 3
    let shift = index * 8
    let data = int data

    this.rxRegister <- this.rxRegister &&& ~~~(0xff <<< shift)
    this.rxRegister <- this.rxRegister |||    (data <<< shift)

    if index = 3 then
      this.rx <- this.rxRegister


  let writeRY this address (data: byte) =
    let index = address &&& 3
    let shift = index * 8
    let data = int data

    this.ryRegister <- this.ryRegister &&& ~~~(0xff <<< shift)
    this.ryRegister <- this.ryRegister |||    (data <<< shift)

    if index = 3 then
      this.ry <- this.ryRegister


  let wireUp bg mmio =
    let readerStub _ = 0uy

    LinearBackground.wireUp bg.linear mmio

    mmio (0x000 + (bg.linear.index * 16)) readerStub (writePA bg)
    mmio (0x001 + (bg.linear.index * 16)) readerStub (writePA bg)
    mmio (0x002 + (bg.linear.index * 16)) readerStub (writePB bg)
    mmio (0x003 + (bg.linear.index * 16)) readerStub (writePB bg)
    mmio (0x004 + (bg.linear.index * 16)) readerStub (writePC bg)
    mmio (0x005 + (bg.linear.index * 16)) readerStub (writePC bg)
    mmio (0x006 + (bg.linear.index * 16)) readerStub (writePD bg)
    mmio (0x007 + (bg.linear.index * 16)) readerStub (writePD bg)
    mmio (0x008 + (bg.linear.index * 16)) readerStub (writeRX bg)
    mmio (0x009 + (bg.linear.index * 16)) readerStub (writeRX bg)
    mmio (0x00a + (bg.linear.index * 16)) readerStub (writeRX bg)
    mmio (0x00b + (bg.linear.index * 16)) readerStub (writeRX bg)
    mmio (0x00c + (bg.linear.index * 16)) readerStub (writeRY bg)
    mmio (0x00d + (bg.linear.index * 16)) readerStub (writeRY bg)
    mmio (0x00e + (bg.linear.index * 16)) readerStub (writeRY bg)
    mmio (0x00f + (bg.linear.index * 16)) readerStub (writeRY bg)
