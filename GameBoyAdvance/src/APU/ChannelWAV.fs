namespace GameBoyAdvance.APU

type ChannelWAV =
  { duration: Duration
    registers: byte[]
    output: bool[]

    amp: byte[][]
    ram: byte[][]

    mutable active: bool
    mutable frequency: int
    mutable cycles: int
    mutable period: int

    mutable bank: int
    mutable count: int
    mutable dimension: int
    mutable shift: int }

module ChannelWAV =

  open System
  open GameBoyAdvance
  open GameBoyAdvance.Platform.Util


  let volumeTable = [| 4; 0; 1; 2 |]


  let frequencyToPeriod f =
    (2048 - f) * 8 * ApuConsts.Single


  let init () =
    let period = frequencyToPeriod 0

    { duration = Duration.init 256
      registers = Array.zeroCreate 8
      output = Array.zeroCreate 2

      amp = ArrayHelper.fill2 2 32 0uy
      ram = ArrayHelper.fill2 2 16 0uy

      active = false
      frequency = 0
      cycles = period
      period = period

      bank = 0
      count = 0
      dimension = 0
      shift = 4 }


  let read wav = IOReader (fun address ->
    wav.ram.[wav.bank ^^^ 1].[address &&& 0x0F]
  )


  let write wav = IOWriter (fun address data ->
    wav.ram.[wav.bank ^^^ 1].[address &&& 0x0f] <- data

    let address = (address <<< 1) &&& 0x1e

    wav.amp.[wav.bank ^^^ 1].[address ||| 0x00] <- (data >>> 4) &&& 0xfuy
    wav.amp.[wav.bank ^^^ 1].[address ||| 0x01] <- (data >>> 0) &&& 0xfuy
  )


  let readReg wav = IOReader (fun address ->
    wav.registers.[address &&& 7]
  )


  let write070 wav = IOWriter (fun _ data ->
    let data = int data
    wav.dimension <- (data >>> 5) &&& 1
    wav.bank <- (data >>> 6) &&& 1

    if (data &&& 0x80) = 0 then
      wav.active <- false

    wav.registers.[0] <- uint8 (data &&& 0xe0)
  )


  let write071 (_: ChannelWAV) = IOWriter (fun _ _ ->
    ()
  )


  let write072 wav = IOWriter (fun _ data ->
    wav.duration |> Duration.write1(data)
  )


  let write073 wav = IOWriter (fun _ data ->
    let data = int data
    wav.shift <- volumeTable.[(data >>> 5) &&& 0x3]

    wav.registers.[3] <- uint8 (data &&& 0xe0)
  )


  let write074 wav = IOWriter (fun _ data ->
    let data = int data
    wav.frequency <- (wav.frequency &&& 0x700) ||| ((data <<< 0) &&& 0x0ff)
    wav.period <- frequencyToPeriod wav.frequency
  )


  let write075 wav = IOWriter (fun _ data ->
    let data = int data
    wav.frequency <- (wav.frequency &&& 0x0ff) ||| ((data <<< 8) &&& 0x700)
    wav.period <- frequencyToPeriod wav.frequency

    if (data &&& 0x80) <> 0 then
      wav.active <- true
      wav.cycles <- wav.period

      wav.duration |> Duration.reset

      wav.count <- 0

    wav.duration |> Duration.write2(uint8 data)

    if (wav.registers.[0] &&& 0x80uy) = 0uy then
      wav.active <- false

    wav.registers.[5] <- uint8 (data &&& 0x40)
  )


  let write076 (_: ChannelWAV) = IOWriter (fun _ _ ->
    ()
  )


  let write077 (_: ChannelWAV) = IOWriter (fun _ _ ->
    ()
  )


  let clockDuration wav =
    if wav.duration |> Duration.clock then
      wav.active <- false


  let tick wav =
    wav.count <- (wav.count + 1) &&& 0x1f

    if wav.count = 0 then
      wav.bank <- wav.bank ^^^ wav.dimension


  let sample wav =
    (int wav.amp.[wav.bank].[wav.count]) >>> wav.shift


  let render t wav =
    if (wav.registers.[0] &&& 0x80uy) <> 0uy then
      let mutable sum = wav.cycles
      wav.cycles <- wav.cycles - t

      if wav.active then
        if wav.cycles < 0 then
          sum <- sum * (sample wav)

          while wav.cycles < 0 do
            wav.cycles <- wav.cycles + wav.period
            wav |> tick

            sum <- sum + (Math.Min(-wav.cycles, wav.period) * (sample wav))

          sum / t
        else
          sample wav
      elif (wav.cycles < 0) then
        let c = (~~~wav.cycles + ApuConsts.Single) / ApuConsts.Single
        wav.cycles <- wav.cycles + (c * ApuConsts.Single)
        wav |> sample
      else
        sample wav
    else
      sample wav
