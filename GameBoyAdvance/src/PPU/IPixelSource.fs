namespace GameBoyAdvance.PPU

type IPixelSource = interface
  abstract member GetColorAddress : int -> int
  abstract member GetPriority : int -> int
  abstract member GetMask : unit -> int
  abstract member IsSemiTransparent : int -> bool
end
