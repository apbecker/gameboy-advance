namespace GameBoyAdvance.APU

type Apu =
  { sound1: ChannelSQ1
    sound2: ChannelSQ2
    sound3: ChannelWAV
    sound4: ChannelNOI
    soundA: ChannelPCM
    soundB: ChannelPCM

    registers: byte[]
    mutable courseTimer: int
    mutable sampleTimer: int
    mutable bias: int
    mutable course: int
    mutable psgShift: int
    volume: int[] }

module Apu =

  open GameBoyAdvance.Platform.Hosting
  open GameBoyAdvance
  open GameBoyAdvance.Memory


  let init () =
    { sound1 = ChannelSQ1.init ()
      sound2 = ChannelSQ2.init ()
      sound3 = ChannelWAV.init ()
      sound4 = ChannelNOI.init ()
      soundA = ChannelPCM.init () // (dma.Channels[1])
      soundB = ChannelPCM.init () // (dma.Channels[2])
      
      registers = Array.zeroCreate 16
      courseTimer = 0
      sampleTimer = 0
      bias = 0
      course = 0
      psgShift = 0
      volume = Array.zeroCreate 2 }


  let private readReg apu = IOReader (fun address ->
    apu.registers.[address &&& 15]
  )


  let private writeReg apu = IOWriter (fun _ _ ->
    ()
  )


  let private read084 apu = IOReader (fun _ ->
    let mutable data = apu.registers.[4] &&& 0x80uy

    if apu.sound1.active then data <- data ||| 0x01uy
    if apu.sound2.active then data <- data ||| 0x02uy
    if apu.sound3.active then data <- data ||| 0x04uy
    if apu.sound4.active then data <- data ||| 0x08uy

    data
  )


  let private write080 apu = IOWriter (fun _ data ->
    apu.registers.[0] <- data &&& 0x77uy

    apu.volume.[1] <- (((int data) >>> 0) &&& 7) + 1
    apu.volume.[0] <- (((int data) >>> 4) &&& 7) + 1
  )


  let private write081 apu = IOWriter (fun _ data ->
    apu.registers.[1] <- data

    apu.sound1.output.[1] <- (data &&& 0x01uy) <> 0uy
    apu.sound2.output.[1] <- (data &&& 0x02uy) <> 0uy
    apu.sound3.output.[1] <- (data &&& 0x04uy) <> 0uy
    apu.sound4.output.[1] <- (data &&& 0x08uy) <> 0uy

    apu.sound1.output.[0] <- (data &&& 0x10uy) <> 0uy
    apu.sound2.output.[0] <- (data &&& 0x20uy) <> 0uy
    apu.sound3.output.[0] <- (data &&& 0x40uy) <> 0uy
    apu.sound4.output.[0] <- (data &&& 0x80uy) <> 0uy
  )


  let private write082 apu = IOWriter (fun _ data ->
    let data = int data
    apu.registers.[2] <- uint8 (data &&& 0x0f)

    apu.psgShift <- data &&& 3
    apu.soundA.shift <- (~~~data >>> 2) &&& 1
    apu.soundB.shift <- (~~~data >>> 3) &&& 1
  )


  let private write083 apu = IOWriter (fun _ data ->
    let data = int data
    apu.registers.[3] <- uint8 (data &&& 0x77)

    apu.soundA.output.[1] <- (data &&& 0x01) <> 0
    apu.soundB.output.[1] <- (data &&& 0x10) <> 0

    apu.soundA.output.[0] <- (data &&& 0x02) <> 0
    apu.soundB.output.[0] <- (data &&& 0x20) <> 0

    apu.soundA.timer <- (data >>> 2) &&& 1
    apu.soundB.timer <- (data >>> 6) &&& 1

    if (data &&& 0x08) <> 0 then ChannelPCM.reset apu.soundA
    if (data &&& 0x80) <> 0 then ChannelPCM.reset apu.soundB
  )


  let private write084 apu = IOWriter (fun _ data ->
    apu.registers.[4] <- data
  )


  let private write088 apu = IOWriter (fun _ data ->
    apu.registers.[8] <- data
    apu.bias <- (apu.bias &&& ~~~0x0ff) ||| (((int data) <<< 0) &&& 0x0ff)
  )


  let private write089 apu = IOWriter (fun _ data ->
    apu.registers.[9] <- data
    apu.bias <- (apu.bias &&& ~~~0x300) ||| (((int data) <<< 8) &&& 0x300)
  )


  let wireUp mmio apu =
    mmio |> MMIO.rw 0x060 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write060 apu.sound1)
    mmio |> MMIO.rw 0x061 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write061 apu.sound1)
    mmio |> MMIO.rw 0x062 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write062 apu.sound1)
    mmio |> MMIO.rw 0x063 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write063 apu.sound1)
    mmio |> MMIO.rw 0x064 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write064 apu.sound1)
    mmio |> MMIO.rw 0x065 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write065 apu.sound1)
    mmio |> MMIO.rw 0x066 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write066 apu.sound1)
    mmio |> MMIO.rw 0x067 (ChannelSQ1.readReg apu.sound1) (ChannelSQ1.write067 apu.sound1)

    mmio |> MMIO.rw 0x068 (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write068 apu.sound2)
    mmio |> MMIO.rw 0x069 (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write069 apu.sound2)
    mmio |> MMIO.rw 0x06a (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06A apu.sound2)
    mmio |> MMIO.rw 0x06b (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06B apu.sound2)
    mmio |> MMIO.rw 0x06c (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06C apu.sound2)
    mmio |> MMIO.rw 0x06d (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06D apu.sound2)
    mmio |> MMIO.rw 0x06e (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06E apu.sound2)
    mmio |> MMIO.rw 0x06f (ChannelSQ2.readReg apu.sound2) (ChannelSQ2.write06F apu.sound2)

    mmio |> MMIO.rw 0x070 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write070 apu.sound3)
    mmio |> MMIO.rw 0x071 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write071 apu.sound3)
    mmio |> MMIO.rw 0x072 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write072 apu.sound3)
    mmio |> MMIO.rw 0x073 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write073 apu.sound3)
    mmio |> MMIO.rw 0x074 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write074 apu.sound3)
    mmio |> MMIO.rw 0x075 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write075 apu.sound3)
    mmio |> MMIO.rw 0x076 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write076 apu.sound3)
    mmio |> MMIO.rw 0x077 (ChannelWAV.readReg apu.sound3) (ChannelWAV.write077 apu.sound3)

    mmio |> MMIO.rw 0x078 (ChannelNOI.readReg apu.sound4) (ChannelNOI.write078 apu.sound4)
    mmio |> MMIO.rw 0x079 (ChannelNOI.readReg apu.sound4) (ChannelNOI.write079 apu.sound4)
    mmio |> MMIO.rw 0x07a (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07A apu.sound4)
    mmio |> MMIO.rw 0x07b (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07B apu.sound4)
    mmio |> MMIO.rw 0x07c (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07C apu.sound4)
    mmio |> MMIO.rw 0x07d (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07D apu.sound4)
    mmio |> MMIO.rw 0x07e (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07E apu.sound4)
    mmio |> MMIO.rw 0x07f (ChannelNOI.readReg apu.sound4) (ChannelNOI.write07F apu.sound4)

    mmio |> MMIO.rw 0x080 (readReg apu) (write080 apu)
    mmio |> MMIO.rw 0x081 (readReg apu) (write081 apu)
    mmio |> MMIO.rw 0x082 (readReg apu) (write082 apu)
    mmio |> MMIO.rw 0x083 (readReg apu) (write083 apu)
    mmio |> MMIO.rw 0x084 (read084 apu) (write084 apu)
    mmio |> MMIO.rw 0x085 (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x086 (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x087 (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x088 (readReg apu) (write088 apu)
    mmio |> MMIO.rw 0x089 (readReg apu) (write089 apu)
    mmio |> MMIO.rw 0x08a (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x08b (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x08c (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x08d (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x08e (readReg apu) (writeReg apu)
    mmio |> MMIO.rw 0x08f (readReg apu) (writeReg apu)

    for i in 0x090 .. 0x09f do
      mmio |> MMIO.rw i (ChannelWAV.read apu.sound3) (ChannelWAV.write apu.sound3)

    mmio |> MMIO.wo 0x0a0 (ChannelPCM.write apu.soundA)
    mmio |> MMIO.wo 0x0a1 (ChannelPCM.write apu.soundA)
    mmio |> MMIO.wo 0x0a2 (ChannelPCM.write apu.soundA)
    mmio |> MMIO.wo 0x0a3 (ChannelPCM.write apu.soundA)

    mmio |> MMIO.wo 0x0a4 (ChannelPCM.write apu.soundB)
    mmio |> MMIO.wo 0x0a5 (ChannelPCM.write apu.soundB)
    mmio |> MMIO.wo 0x0a6 (ChannelPCM.write apu.soundB)
    mmio |> MMIO.wo 0x0a7 (ChannelPCM.write apu.soundB)


  let getPsgOutput speaker sq1 sq2 wav noi apu =
    let mutable sample = 0
    if (apu.sound1.output.[speaker]) then sample <- sample + sq1
    if (apu.sound2.output.[speaker]) then sample <- sample + sq2
    if (apu.sound3.output.[speaker]) then sample <- sample + wav
    if (apu.sound4.output.[speaker]) then sample <- sample + noi

    match apu.psgShift with
    | 0 -> (sample * apu.volume.[speaker]) >>> 2
    | 1 -> (sample * apu.volume.[speaker]) >>> 1
    | 2 -> (sample * apu.volume.[speaker]) >>> 0
    | 3 -> 0
    | n -> failwithf "invalid psg shift: %d" n


  let getSoundAOutput (speaker: int) apu =
    if apu.soundA.output.[speaker] then
      apu.soundA.level >>> apu.soundA.shift
    else
      0


  let getSoundBOutput (speaker: int) apu =
    if apu.soundB.output.[speaker] then
      apu.soundB.level >>> apu.soundB.shift
    else
      0


  let clamp n =
    let min = ~~~511
    let max = 511

    if n < min then min
    elif n > max then max
    else n


  let sample audio apu =
    let sq1 = apu.sound1 |> ChannelSQ1.render(ApuConsts.Frequency)
    let sq2 = apu.sound2 |> ChannelSQ2.render(ApuConsts.Frequency)
    let wav = apu.sound3 |> ChannelWAV.render(ApuConsts.Frequency)
    let noi = apu.sound4 |> ChannelNOI.render(ApuConsts.Frequency)

    for i in 0 .. 1 do
      let sample = // 0x200 - bias
        (apu |> getPsgOutput i sq1 sq2 wav noi) +
        (apu |> getSoundAOutput i) +
        (apu |> getSoundBOutput i)

      AudioSink.run audio ((clamp sample) * 64)


  let clock amount audio apu =
    apu.courseTimer <- apu.courseTimer + (amount * 512)

    while apu.courseTimer >= ApuConsts.Frequency do
      apu.courseTimer <- apu.courseTimer - ApuConsts.Frequency

      match apu.course with
      | 0
      | 4 ->
        apu.sound1 |> ChannelSQ1.clockDuration
        apu.sound2 |> ChannelSQ2.clockDuration
        apu.sound3 |> ChannelWAV.clockDuration
        apu.sound4 |> ChannelNOI.clockDuration

      | 2
      | 6 ->
        apu.sound1 |> ChannelSQ1.clockDuration
        apu.sound2 |> ChannelSQ2.clockDuration
        apu.sound3 |> ChannelWAV.clockDuration
        apu.sound4 |> ChannelNOI.clockDuration

        apu.sound1 |> ChannelSQ1.clockSweep

      | 7 ->
        apu.sound1 |> ChannelSQ1.clockEnvelope
        apu.sound2 |> ChannelSQ2.clockEnvelope
        apu.sound4 |> ChannelNOI.clockEnvelope

      | _ -> ()

      apu.course <- (apu.course + 1) &&& 7

    apu.sampleTimer <- apu.sampleTimer + (amount * ApuConsts.Single)

    while apu.sampleTimer >= ApuConsts.Frequency do
      apu.sampleTimer <- apu.sampleTimer - ApuConsts.Frequency
      apu |> sample audio
