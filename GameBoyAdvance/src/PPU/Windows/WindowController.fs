namespace GameBoyAdvance.PPU.Windows

type WindowController =
  { window0: Window
    window1: Window
    window2: SpriteWindow
    mutable outsideFlags: WindowFlags }

module WindowController =

  open GameBoyAdvance


  let init () =
    { window0 = Window.init ()
      window1 = Window.init ()
      window2 = SpriteWindow.init ()

      outsideFlags = WindowFlags.None }


  let getFlags x y ctrl =
    let enabled = ctrl.window0.enabled || ctrl.window1.enabled || ctrl.window2.enabled
    if not enabled then
      WindowFlags.All
    elif ctrl.window0 |> Window.inRange x y then
      ctrl.window0.insideFlags
    elif ctrl.window1 |> Window.inRange x y then
      ctrl.window1.insideFlags
    elif ctrl.window2 |> SpriteWindow.inRange x y then
      ctrl.window2.insideFlags
    else
      ctrl.outsideFlags


  let read ctrl = IOReader (fun address ->
    match address with
    | 0x48 -> uint8 ctrl.window0.insideFlags
    | 0x49 -> uint8 ctrl.window1.insideFlags
    | 0x4a -> uint8 ctrl.outsideFlags
    | 0x4b -> uint8 ctrl.window2.insideFlags
    | _    -> failwithf "Unhandled read 0x%08x" address
  )


  let write ctrl = IOWriter (fun address data ->
    let data = int data
    match address with
    | 0x40 -> ctrl.window0.x2 <- data
    | 0x41 -> ctrl.window0.x1 <- data
    | 0x42 -> ctrl.window1.x2 <- data
    | 0x43 -> ctrl.window1.x1 <- data
    | 0x44 -> ctrl.window0.y2 <- data
    | 0x45 -> ctrl.window0.y1 <- data
    | 0x46 -> ctrl.window1.y2 <- data
    | 0x47 -> ctrl.window1.y1 <- data
    | 0x48 -> ctrl.window0.insideFlags <- (data &&& 0x3f) |> enum<WindowFlags>
    | 0x49 -> ctrl.window1.insideFlags <- (data &&& 0x3f) |> enum<WindowFlags>
    | 0x4a -> ctrl.outsideFlags <- (data &&& 0x3f) |> enum<WindowFlags>
    | 0x4b -> ctrl.window2.insideFlags <- (data &&& 0x3f) |> enum<WindowFlags>
    | _    -> failwithf "Unhandled write 0x%08x, 0x%02x" address data
  )


  let setEnabled w0 w1 w2 ctrl =
    ctrl.window0.enabled <- w0
    ctrl.window1.enabled <- w1
    ctrl.window2.enabled <- w2


  let getSpriteWindow ctrl =
    ctrl.window2
