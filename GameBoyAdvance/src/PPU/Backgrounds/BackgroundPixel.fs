namespace GameBoyAdvance.PPU.Backgrounds

[<Struct>]
type BackgroundPixel =
  { mutable opaque: bool
    mutable colorAddress: int }

module BackgroundPixel =

  let init () =
    { opaque = false
      colorAddress = 0 }
