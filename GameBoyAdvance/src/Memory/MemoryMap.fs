namespace GameBoyAdvance.Memory

open GameBoyAdvance
open GameBoyAdvance.CPU
open GameBoyAdvance.Platform
open GameBoyAdvance.Platform.Util
open GameBoyAdvance.Platform.Exceptions

type MemoryMap =
  { eram: Memory
    iram: Memory
    mmio: MMIO
    oram: Memory
    pram: Memory
    vram: Memory

    bios: BIOS
    gamePak: GamePak }

module MemoryMap =

  let init bios gamePak =
    { eram = ERAM.init ()
      iram = IRAM.init ()
      mmio = MMIO.init ()
      oram = ORAM.init ()
      pram = PRAM.init ()
      vram = VRAM.init ()
      bios = bios
      gamePak = gamePak }


  let private timingTableByte = [| 1; 1; 3; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1 |]
  let private timingTableHalf = [| 1; 1; 3; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1; 1 |]
  let private timingTableWord = [| 1; 1; 6; 1; 1; 2; 2; 1; 1; 1; 1; 1; 1; 1; 1; 1 |]


  let timingTable = [|
    timingTableByte
    timingTableHalf
    timingTableWord
  |]


  let read cpu addClock size address memory =
    let area = (address >>> 24) &&& 15

    match area with
    | 0x0
    | 0x1 -> memory.bios |> BIOS.read cpu size address
    | 0x2 -> memory.eram |> ERAM.read size address
    | 0x3 -> memory.iram |> IRAM.read size address
    | 0x4 -> memory.mmio |> MMIO.read size address
    | 0x5 -> memory.pram |> PRAM.read size address
    | 0x6 -> memory.vram |> VRAM.read size address
    | 0x7 -> memory.oram |> ORAM.read size address
    | 0x8
    | 0x9
    | 0xa
    | 0xb
    | 0xc
    | 0xd -> memory.gamePak |> GamePak.readRom addClock size address
    | 0xe
    | _   -> memory.gamePak |> GamePak.readRam addClock size address


  let timedRead cpu addClock size address memory =
    let area = (address >>> 24) &&& 15

    match size with
    | Size.Byte -> addClock timingTableByte.[area]
    | Size.Half -> addClock timingTableHalf.[area]
    | Size.Word -> addClock timingTableWord.[area]

    memory |> read cpu addClock size address


  let write addClock size address data memory =
    let area = (address >>> 24) &&& 15

    match area with
    | 0x0
    | 0x1 -> memory.bios |> BIOS.write size address data
    | 0x2 -> memory.eram |> ERAM.write size address data
    | 0x3 -> memory.iram |> IRAM.write size address data
    | 0x4 -> memory.mmio |> MMIO.write size address data
    | 0x5 -> memory.pram |> PRAM.write size address data
    | 0x6 -> memory.vram |> VRAM.write size address data
    | 0x7 -> memory.oram |> ORAM.write size address data
    | 0x8
    | 0x9
    | 0xa
    | 0xb
    | 0xc
    | 0xd -> memory.gamePak |> GamePak.writeRom addClock size address data
    | 0xe
    | _   -> memory.gamePak |> GamePak.writeRam addClock size address data


  let timedWrite addClock size address data memory =
    let area = (address >>> 24) &&& 15

    match size with
    | Size.Byte -> addClock timingTableByte.[area]
    | Size.Half -> addClock timingTableHalf.[area]
    | Size.Word -> addClock timingTableWord.[area]

    memory |> write addClock size address data
