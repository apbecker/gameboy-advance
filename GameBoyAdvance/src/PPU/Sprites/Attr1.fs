namespace GameBoyAdvance.PPU.Sprites

type Attr1 =
  { x: int
    param: int
    xFlip: bool
    yFlip: bool
    size: int }

module Attr1 =

  let fromPrimitive value =
    { x     = (value &&& 0x01ff) >>> 0
      param = (value &&& 0x3e00) >>> 9
      xFlip = (value &&& 0x1000) <>  0
      yFlip = (value &&& 0x2000) <>  0
      size  = (value &&& 0xc000) >>> 14 }
