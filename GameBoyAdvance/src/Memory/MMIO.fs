namespace GameBoyAdvance.Memory

open GameBoyAdvance

type MMIO =
  { readers: IOReader[]
    writers: IOWriter[] }

module MMIO =

  open GameBoyAdvance


  let [<Literal>] SIZE = 0x400
  let [<Literal>] MASK = 0x3ff


  let private readOpenBus = IOReader (fun _ -> 0uy)


  let private writeOpenBus = IOWriter (fun _ _ -> ())


  let init () =
    { readers = Seq.toArray <| Seq.init SIZE (fun _ -> readOpenBus)
      writers = Seq.toArray <| Seq.init SIZE (fun _ -> writeOpenBus) }


  let readByte address mmio =
    if address > 0x040003ff then
      0
    else
      let address = address &&& MASK
      let (IOReader(f)) = mmio.readers.[address]
      int (f address)
  

  let readHalf address mmio =
    let lower = mmio |> readByte (address &&& ~~~1)
    let upper = mmio |> readByte (address |||    1)
    lower ||| (upper <<< 8)


  let readWord address mmio =
    let lower = mmio |> readHalf (address &&& ~~~2)
    let upper = mmio |> readHalf (address |||    2)
    lower ||| (upper <<< 16)


  let read size address mmio =
    match size with
    | Size.Byte -> mmio |> readByte address
    | Size.Half -> mmio |> readHalf address
    | Size.Word -> mmio |> readWord address


  let writeByte address data mmio =
    if address > 0x040003ff then
      ()
    else
      let address = address &&& MASK
      let (IOWriter(f)) = mmio.writers.[address]
      f address (uint8 data)


  let writeHalf address data mmio =
    mmio |> writeByte (address &&& ~~~1) (data >>> 0)
    mmio |> writeByte (address |||    1) (data >>> 8)


  let writeWord address data mmio =
    mmio |> writeHalf (address &&& ~~~2) (data >>> 0)
    mmio |> writeHalf (address |||    2) (data >>> 16)


  let write size address data mmio =
    match size with
    | Size.Byte -> mmio |> writeByte address data
    | Size.Half -> mmio |> writeHalf address data
    | Size.Word -> mmio |> writeWord address data


  let ro address reader mmio =
    mmio.readers.[address] <- reader


  let wo address writer mmio =
    mmio.writers.[address] <- writer


  let rw address reader writer mmio =
    mmio |> ro address reader
    mmio |> wo address writer
