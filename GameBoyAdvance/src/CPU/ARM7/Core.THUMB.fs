module GameBoyAdvance.Processors.ARM7.THUMB

open GameBoyAdvance
open GameBoyAdvance.Platform.Util

type ThumbOp =
  | ThumbOp of (ReadFunction -> WriteFunction -> Core -> unit)

module ThumbOp =

  let call reader writer core (ThumbOp f) =
    f reader writer core


// #region Opcodes


let thumbOpShift = ThumbOp (fun _ _ core ->
  // lsl rd, rm, #nn
  let rd = (core.code >>> 0) &&& 0x07
  let rm = (core.code >>> 3) &&& 0x07
  let nn = (core.code >>> 6) &&& 0x1f

  match ((core.code >>> 11) &&& 0x03) with
  | 0x00 -> core.registers.[rd].value <- core.Mov (core.LSL core.registers.[rm].value nn)
  | 0x01 -> core.registers.[rd].value <- core.Mov (core.LSR core.registers.[rm].value (if nn = 0 then 32 else nn))
  | 0x02 -> core.registers.[rd].value <- core.Mov (core.ASR core.registers.[rm].value (if nn = 0 then 32 else nn))
  | _    -> ()
)


let thumbOpAdjust = ThumbOp (fun _ _ core ->
  // add rd, rn, rm
  let rd = (core.code >>> 0) &&& 0x07
  let rn = (core.code >>> 3) &&& 0x07
  let rm = (core.code >>> 6) &&& 0x07

  let a = core.registers.[rn].value

  match ((core.code >>> 9) &&& 0x03) with
  | 0 -> core.registers.[rd].value <- core.Add a core.registers.[rm].value
  | 1 -> core.registers.[rd].value <- core.Sub a core.registers.[rm].value
  | 2 -> core.registers.[rd].value <- core.Add a rm
  | _ -> core.registers.[rd].value <- core.Sub a rm
)


let thumbOpMovImm = ThumbOp (fun _ _ core ->
  // mov rd, #nn
  let rd = (core.code >>> 8) &&& 0x07
  let nn = (core.code >>> 0) &&& 0xff
  core.registers.[rd].value <- core.Mov nn
)


let thumbOpCmpImm = ThumbOp (fun _ _ core ->
  // cmp rn, #nn
  let rd = (core.code >>> 8) &&& 0x07
  let nn = (core.code >>> 0) &&& 0xff
  core.Sub core.registers.[rd].value nn |> ignore
)


let thumbOpAddImm = ThumbOp (fun _ _ core ->
  // add rd, #nn
  let rd = (core.code >>> 8) &&& 0x07
  let nn = (core.code >>> 0) &&& 0xff
  core.registers.[rd].value <- core.Add core.registers.[rd].value nn
)


let thumbOpSubImm = ThumbOp (fun _ _ core ->
  // sub rd, #nn
  let rd = (core.code >>> 8) &&& 0x07
  let nn = (core.code >>> 0) &&& 0xff
  core.registers.[rd].value <- core.Sub core.registers.[rd].value nn
)


let thumbOpAlu = ThumbOp (fun _ _ core ->
  let rd = (core.code >>> 0) &&& 7
  let rn = (core.code >>> 3) &&& 7

  match ((core.code >>> 6) &&& 15) with
  | 0x0 -> core.registers.[rd].value <- core.Mov(core.registers.[rd].value &&& core.registers.[rn].value)
  | 0x1 -> core.registers.[rd].value <- core.Mov(core.registers.[rd].value ^^^ core.registers.[rn].value)
  | 0x2 -> core.registers.[rd].value <- core.Mov(core.LSL core.registers.[rd].value (core.registers.[rn].value &&& 0xff))
  | 0x3 -> core.registers.[rd].value <- core.Mov(core.LSR core.registers.[rd].value (core.registers.[rn].value &&& 0xff))
  | 0x4 -> core.registers.[rd].value <- core.Mov(core.ASR core.registers.[rd].value (core.registers.[rn].value &&& 0xff))
  | 0x5 -> core.registers.[rd].value <- core.AddCarry core.registers.[rd].value core.registers.[rn].value core.cpsr.c
  | 0x6 -> core.registers.[rd].value <- core.SubCarry core.registers.[rd].value core.registers.[rn].value core.cpsr.c
  | 0x7 -> core.registers.[rd].value <- core.Mov(core.ROR core.registers.[rd].value (core.registers.[rn].value &&& 0xff))
  | 0x8 -> core.Mov (core.registers.[rd].value &&& core.registers.[rn].value) |> ignore
  | 0x9 -> core.registers.[rd].value <- core.Sub 0 core.registers.[rn].value
  | 0xa -> core.Sub core.registers.[rd].value core.registers.[rn].value |> ignore
  | 0xb -> core.Add core.registers.[rd].value core.registers.[rn].value |> ignore
  | 0xc -> core.registers.[rd].value <- core.Mov(core.registers.[rd].value ||| core.registers.[rn].value)
  | 0xd -> core.registers.[rd].value <- core.Mul core.registers.[rd].value core.registers.[rn].value 0
  | 0xe -> core.registers.[rd].value <- core.Mov(core.registers.[rd].value &&& ~~~core.registers.[rn].value)
  | _   -> core.registers.[rd].value <- core.Mov(~~~core.registers.[rn].value)
)


let thumbOpAddHi = ThumbOp (fun _ _ core ->
  let rd = ((core.code &&& (1 <<< 7)) >>> 4) ||| (core.code &&& 0x7)
  let rm = (core.code >>> 3) &&& 0xF

  core.registers.[rd].value <- core.registers.[rd].value + core.registers.[rm].value

  if (rd = 15) then
    core.registers.[rd].value <- core.registers.[rd].value &&& (~~~1)
    core.pipeline.refresh <- true
)


let thumbOpCmpHi = ThumbOp (fun _ _ core ->
  let rd = ((core.code &&& (1 <<< 7)) >>> 4) ||| (core.code &&& 0x7)
  let rm = (core.code >>> 3) &&& 0xF

  core.Sub core.registers.[rd].value core.registers.[rm].value |> ignore
)


let thumbOpMovHi = ThumbOp (fun _ _ core ->
  let rd = ((core.code &&& (1 <<< 7)) >>> 4) ||| (core.code &&& 0x7)
  let rm = (core.code >>> 3) &&& 0xF

  core.registers.[rd].value <- core.registers.[rm].value

  if (rd = 15) then
    core.registers.[rd].value <- core.registers.[rd].value &&& (~~~1)
    core.pipeline.refresh <- true
)


let thumbOpBx = ThumbOp (fun _ _ core ->
  let rm = (core.code >>> 3) &&& 15
  core.cpsr.t <- core.registers.[rm].value &&& 1

  core.pc.value <- core.registers.[rm].value &&& (~~~1)
  core.pipeline.refresh <- true
)


let thumbOpLdrPc = ThumbOp (fun reader _ core ->
  core.registers.[(core.code >>> 8) &&& 0x7].value <- core.ReadWord reader ((core.pc.value &&& (~~~2)) + (core.code &&& 0xFF) * 4)
  core.cycles <- core.cycles + 1
)


let private thumbAddress core =
  core.registers.[(core.code >>> 3) &&& 0x7].value + core.registers.[(core.code >>> 6) &&& 0x7].value


let thumbOpStrReg = ThumbOp (fun _ writer core ->
  core.Write writer Size.Word (thumbAddress core) (core.registers.[core.code &&& 0x7].value)
)


let thumbOpStrhReg = ThumbOp (fun reader writer core ->
  let data = (core.registers.[core.code &&& 0x7].value &&& 0xFFFF)
  core.Write writer Size.Half (thumbAddress core) data
)


let thumbOpStrbReg = ThumbOp (fun reader writer core ->
  let data = (core.registers.[core.code &&& 0x7].value &&& 0xFF)
  core.Write writer Size.Byte (thumbAddress core) data
)


let thumbOpLdrsbReg = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadByteSignExtended reader (thumbAddress core)
  core.cycles <- core.cycles + 1
)


let thumbOpLdrReg = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadWord reader (thumbAddress core)
  core.cycles <- core.cycles + 1
)


let thumbOpLdrhReg = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadHalf reader (thumbAddress core)
  core.cycles <- core.cycles + 1
)


let thumbOpLdrbReg = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadByte reader (thumbAddress core)
  core.cycles <- core.cycles + 1
)


let thumbOpLdrshReg = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadHalfSignExtended reader (thumbAddress core)
  core.cycles <- core.cycles + 1
)


let thumbOpStrImm = ThumbOp (fun reader writer core ->
  core.Write writer Size.Word (core.registers.[(core.code >>> 3) &&& 0x7].value + ((core.code >>> 6) &&& 0x1F) * 4) (core.registers.[core.code &&& 0x7].value)
)


let thumbOpLdrImm = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadWord reader (core.registers.[(core.code >>> 3) &&& 0x7].value + ((core.code >>> 6) &&& 0x1F) * 4)
  core.cycles <- core.cycles + 1
)


let thumbOpStrbImm = ThumbOp (fun reader writer core ->
  let addr = (core.registers.[(core.code >>> 3) &&& 0x7].value + ((core.code >>> 6) &&& 0x1F))
  let data = (core.registers.[core.code &&& 0x7].value &&& 0xFF)
  core.Write writer Size.Byte addr data
)


let thumbOpLdrbImm = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 0x7].value <- core.ReadByte reader (core.registers.[(core.code >>> 3) &&& 0x7].value + ((core.code >>> 6) &&& 0x1F))
  core.cycles <- core.cycles + 1
)


let thumbOpStrhImm = ThumbOp (fun reader writer core ->
  let addr = (core.registers.[(core.code >>> 3) &&& 7].value + ((core.code >>> 6) &&& 0x1F) * 2)
  let data = (core.registers.[core.code &&& 0x7].value &&& 0xFFFF)
  core.Write writer Size.Half addr data
)


let thumbOpLdrhImm = ThumbOp (fun reader writer core ->
  core.registers.[core.code &&& 7].value <- core.ReadHalf reader (core.registers.[(core.code >>> 3) &&& 7].value + ((core.code >>> 6) &&& 0x1f) * 2)
  core.cycles <- core.cycles + 1
)


let thumbOpStrSp = ThumbOp (fun reader writer core ->
  core.Write writer Size.Word (core.sp.value + ((core.code <<< 2) &&& 0x3fc)) (core.registers.[(core.code >>> 8) &&& 7].value)
)


let thumbOpLdrSp = ThumbOp (fun reader writer core ->
  core.registers.[(core.code >>> 8) &&& 7].value <- core.ReadWord reader (core.sp.value + ((core.code <<< 2) &&& 0x3fc))
)


let thumbOpAddPc = ThumbOp (fun reader writer core ->
  core.registers.[(core.code >>> 8) &&& 7].value <- (core.pc.value &&& (~~~2)) + ((core.code <<< 2) &&& 0x3fc)
)


let thumbOpAddSp = ThumbOp (fun reader writer core ->
  core.registers.[(core.code >>> 8) &&& 7].value <- (core.sp.value &&& (~~~0)) + ((core.code <<< 2) &&& 0x3fc)
)


let thumbOpSubSp = ThumbOp (fun reader writer core ->
  if ((core.code &&& (1 <<< 7)) <> 0) then
    core.sp.value <- core.sp.value - ((core.code <<< 2) &&& 0x1fc)
  else
    core.sp.value <- core.sp.value + ((core.code <<< 2) &&& 0x1fc)
)


let thumbOpPush = ThumbOp (fun reader writer core ->
  if ((core.code &&& 0x100) <> 0) then
    core.sp.value <- core.sp.value - 4
    core.Write writer Size.Word core.sp.value core.lr.value

  for i in 7 .. -1 .. 0 do // (let i = 7 i >= 0 i--)
    if (((core.code >>> i) &&& 1) <> 0) then
      core.sp.value <- core.sp.value - 4
      core.Write writer Size.Word core.sp.value core.registers.[i].value
)


let thumbOpPop = ThumbOp (fun reader writer core ->
  for i in 0 .. 7 do
    if (((core.code >>> i) &&& 1) <> 0) then
      core.registers.[i].value <- core.ReadWord reader (core.sp.value)
      core.sp.value <- core.sp.value + 4

  if ((core.code &&& 0x100) <> 0) then
    core.pc.value <- core.ReadWord reader (core.sp.value) &&& (~~~1)
    core.sp.value <- core.sp.value + 4
    core.pipeline.refresh <- true

  core.cycles <- core.cycles + 1
)


let thumbOpStmia = ThumbOp (fun reader writer core ->
  let rn = (core.code >>> 8) &&& 0x07

  for i in 0 .. 7 do
    if (((core.code >>> i) &&& 1) <> 0) then
      core.Write writer Size.Word (core.registers.[rn].value &&& (~~~3)) core.registers.[i].value
      core.registers.[rn].value <- core.registers.[rn].value + 4
)


let thumbOpLdmia = ThumbOp (fun reader writer core ->
  let rn = (core.code >>> 8) &&& 0x07

  let mutable address = core.registers.[rn].value

  for i in 0 .. 7 do
    if (((core.code >>> i) &&& 1) <> 0) then
      core.registers.[i].value <- core.Read reader Size.Word (address &&& (~~~3))
      address <- address + 4

  core.registers.[rn].value <- address
)


let thumbOpBCond = ThumbOp (fun reader writer core ->
  if core.GetCondition (core.code >>> 8) then
    core.pc.value <- core.pc.value + ((MathHelper.signExtend 8 core.code) <<< 1)
    core.pipeline.refresh <- true
)


let thumbOpSwi = ThumbOp (fun reader writer core ->
  core.Isr Mode.SVC Vector.SWI
)


let thumbOpB = ThumbOp (fun reader writer core ->
  core.pc.value <- core.pc.value + ((MathHelper.signExtend 11 core.code) <<< 1)
  core.pipeline.refresh <- true
)


let thumbOpBl1 = ThumbOp (fun reader writer core ->
  core.lr.value <- core.pc.value + ((MathHelper.signExtend 11 core.code) <<< 12)
)


let thumbOpBl2 = ThumbOp (fun reader writer core ->
  let link = core.pc.value - 2
  core.pc.value <- core.lr.value + ((core.code &&& 0x7ff) <<< 1)
  core.lr.value <- link ||| 1
  core.pipeline.refresh <- true
)


let thumbOpUnd = ThumbOp (fun reader writer core ->
  core.Isr Mode.UND Vector.UND
)


// #endregion


let thumbTable = [|
  thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;    thumbOpShift
  thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;    thumbOpShift
  thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;   thumbOpShift;    thumbOpShift
  thumbOpAdjust;  thumbOpAdjust;  thumbOpAdjust;  thumbOpAdjust;  thumbOpAdjust;  thumbOpAdjust;  thumbOpAdjust;   thumbOpAdjust
  thumbOpMovImm;  thumbOpMovImm;  thumbOpMovImm;  thumbOpMovImm;  thumbOpMovImm;  thumbOpMovImm;  thumbOpMovImm;   thumbOpMovImm
  thumbOpCmpImm;  thumbOpCmpImm;  thumbOpCmpImm;  thumbOpCmpImm;  thumbOpCmpImm;  thumbOpCmpImm;  thumbOpCmpImm;   thumbOpCmpImm
  thumbOpAddImm;  thumbOpAddImm;  thumbOpAddImm;  thumbOpAddImm;  thumbOpAddImm;  thumbOpAddImm;  thumbOpAddImm;   thumbOpAddImm
  thumbOpSubImm;  thumbOpSubImm;  thumbOpSubImm;  thumbOpSubImm;  thumbOpSubImm;  thumbOpSubImm;  thumbOpSubImm;   thumbOpSubImm
  thumbOpAlu;     thumbOpAlu;     thumbOpAlu;     thumbOpAlu;     thumbOpAddHi;   thumbOpCmpHi;   thumbOpMovHi;    thumbOpBx
  thumbOpLdrPc;   thumbOpLdrPc;   thumbOpLdrPc;   thumbOpLdrPc;   thumbOpLdrPc;   thumbOpLdrPc;   thumbOpLdrPc;    thumbOpLdrPc
  thumbOpStrReg;  thumbOpStrReg;  thumbOpStrhReg; thumbOpStrhReg; thumbOpStrbReg; thumbOpStrbReg; thumbOpLdrsbReg; thumbOpLdrsbReg
  thumbOpLdrReg;  thumbOpLdrReg;  thumbOpLdrhReg; thumbOpLdrhReg; thumbOpLdrbReg; thumbOpLdrbReg; thumbOpLdrshReg; thumbOpLdrshReg
  thumbOpStrImm;  thumbOpStrImm;  thumbOpStrImm;  thumbOpStrImm;  thumbOpStrImm;  thumbOpStrImm;  thumbOpStrImm;   thumbOpStrImm
  thumbOpLdrImm;  thumbOpLdrImm;  thumbOpLdrImm;  thumbOpLdrImm;  thumbOpLdrImm;  thumbOpLdrImm;  thumbOpLdrImm;   thumbOpLdrImm
  thumbOpStrbImm; thumbOpStrbImm; thumbOpStrbImm; thumbOpStrbImm; thumbOpStrbImm; thumbOpStrbImm; thumbOpStrbImm;  thumbOpStrbImm
  thumbOpLdrbImm; thumbOpLdrbImm; thumbOpLdrbImm; thumbOpLdrbImm; thumbOpLdrbImm; thumbOpLdrbImm; thumbOpLdrbImm;  thumbOpLdrbImm
  thumbOpStrhImm; thumbOpStrhImm; thumbOpStrhImm; thumbOpStrhImm; thumbOpStrhImm; thumbOpStrhImm; thumbOpStrhImm;  thumbOpStrhImm
  thumbOpLdrhImm; thumbOpLdrhImm; thumbOpLdrhImm; thumbOpLdrhImm; thumbOpLdrhImm; thumbOpLdrhImm; thumbOpLdrhImm;  thumbOpLdrhImm
  thumbOpStrSp;   thumbOpStrSp;   thumbOpStrSp;   thumbOpStrSp;   thumbOpStrSp;   thumbOpStrSp;   thumbOpStrSp;    thumbOpStrSp
  thumbOpLdrSp;   thumbOpLdrSp;   thumbOpLdrSp;   thumbOpLdrSp;   thumbOpLdrSp;   thumbOpLdrSp;   thumbOpLdrSp;    thumbOpLdrSp
  thumbOpAddPc;   thumbOpAddPc;   thumbOpAddPc;   thumbOpAddPc;   thumbOpAddPc;   thumbOpAddPc;   thumbOpAddPc;    thumbOpAddPc
  thumbOpAddSp;   thumbOpAddSp;   thumbOpAddSp;   thumbOpAddSp;   thumbOpAddSp;   thumbOpAddSp;   thumbOpAddSp;    thumbOpAddSp
  thumbOpSubSp;   thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpPush;    thumbOpPush;    thumbOpUnd;      thumbOpUnd
  thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpPop;     thumbOpPop;     thumbOpUnd;      thumbOpUnd
  thumbOpStmia;   thumbOpStmia;   thumbOpStmia;   thumbOpStmia;   thumbOpStmia;   thumbOpStmia;   thumbOpStmia;    thumbOpStmia
  thumbOpLdmia;   thumbOpLdmia;   thumbOpLdmia;   thumbOpLdmia;   thumbOpLdmia;   thumbOpLdmia;   thumbOpLdmia;    thumbOpLdmia
  thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;    thumbOpBCond
  thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpBCond;   thumbOpUnd;      thumbOpSwi
  thumbOpB;       thumbOpB;       thumbOpB;       thumbOpB;       thumbOpB;       thumbOpB;       thumbOpB;        thumbOpB
  thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpUnd;     thumbOpUnd;      thumbOpUnd
  thumbOpBl1;     thumbOpBl1;     thumbOpBl1;     thumbOpBl1;     thumbOpBl1;     thumbOpBl1;     thumbOpBl1;      thumbOpBl1
  thumbOpBl2;     thumbOpBl2;     thumbOpBl2;     thumbOpBl2;     thumbOpBl2;     thumbOpBl2;     thumbOpBl2;      thumbOpBl2
|]


let thumbDecode code =
  (code >>> 8) &&& 0xff


let thumbStep reader core =
  core.pc.value <- core.pc.value + 2

  core.pipeline.execute <- core.pipeline.decode
  core.pipeline.decode <- core.pipeline.fetch
  core.pipeline.fetch <- core.Read reader Size.Half (core.pc.value &&& (~~~1))


let thumbExecute reader writer core =
  if core.pipeline.refresh then
    core.pipeline.refresh <- false
    core.pipeline.fetch <- core.Read reader Size.Half (core.pc.value &&& (~~~1))

    core |> thumbStep reader

  core |> thumbStep reader

  if (core.interrupt && core.cpsr.i = 0) then // irq after pipeline initialized in correct mode
    core.Isr Mode.IRQ Vector.IRQ
    core.lr.value <- core.lr.value + 2
  else
    core.code <- core.pipeline.execute
    thumbTable.[thumbDecode core.code] |> ThumbOp.call reader writer core
