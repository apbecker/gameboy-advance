namespace GameBoyAdvance.APU

type ChannelSQ1 =
  { duration: Duration
    envelope: Envelope
    registers: byte[]

    mutable active: bool
    mutable frequency: int
    mutable cycles: int
    mutable period: int

    mutable output: bool[]

    mutable dutyForm: int
    mutable dutyStep: int

    mutable sweepEnable: bool
    mutable sweepCycles: int
    mutable sweepDelta: int
    mutable sweepShift: int
    mutable sweepShadow: int
    mutable sweepPeriod: int }

module ChannelSQ1 =

  open GameBoyAdvance


  let dutyTable = [|
    [| 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x00 |]
    [| 0x00; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x00 |]
    [| 0x00; 0x1f; 0x1f; 0x1f; 0x1f; 0x00; 0x00; 0x00 |]
    [| 0x1f; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x1f |]
  |]


  let frequencyToPeriod (f: int) =
    (2048 - f) * 16 * ApuConsts.Single


  let init () =
    let period = frequencyToPeriod 0

    { duration = Duration.init 64
      envelope = Envelope.init ()
      registers = Array.zeroCreate 8

      active = false
      frequency = 0
      cycles = period
      period = period

      output = Array.zeroCreate 2

      dutyForm = 0
      dutyStep = 7

      sweepEnable = false
      sweepCycles = 0
      sweepDelta = 1
      sweepShift = 0
      sweepShadow = 0
      sweepPeriod = 0 }


  let readReg sq1 = IOReader (fun address ->
    sq1.registers.[address &&& 7]
  )


  let write060 sq1 = IOWriter (fun address data ->
    let data = int data
    sq1.sweepPeriod <- (data >>> 4) &&& 7
    sq1.sweepDelta <- 1 - ((data >>> 2) &&& 2)
    sq1.sweepShift <- (data >>> 0) &&& 7

    sq1.registers.[0] <- uint8 (data &&& 0x7f)
  )


  let write061 sq1 = IOWriter (fun _ _ ->
    ()
  )


  let write062 sq1 = IOWriter (fun _ data ->
    let data = int data
    sq1.dutyForm <- data >>> 6
    sq1.duration |> Duration.write1(uint8 data)

    sq1.registers.[2] <- uint8 (data &&& 0xc0)
  )


  let write063 sq1 = IOWriter (fun _ data ->
    sq1.envelope |> Envelope.write(data)

    sq1.registers.[3] <- data
  )


  let write064 sq1 = IOWriter (fun _ data ->
    let data = int data
    sq1.frequency <- (sq1.frequency &&& 0x700) ||| ((data <<< 0) &&& 0x0ff)
    sq1.period <- frequencyToPeriod sq1.frequency
  )


  let write065 sq1 = IOWriter (fun _ data ->
    let data = int data
    sq1.frequency <- (sq1.frequency &&& 0x0ff) ||| ((data <<< 8) &&& 0x700)
    sq1.period <- frequencyToPeriod sq1.frequency

    if (data &&& 0x80) <> 0 then
      sq1.active <- true
      sq1.cycles <- sq1.period

      sq1.duration |> Duration.reset
      sq1.envelope |> Envelope.reset

      sq1.sweepShadow <- sq1.frequency
      sq1.sweepCycles <- sq1.sweepPeriod
      sq1.sweepEnable <- sq1.sweepPeriod <> 0 || sq1.sweepShift <> 0

      sq1.dutyStep <- 7

    sq1.duration |> Duration.write2(byte data)

    if (sq1.registers.[3] &&& 0xf8uy) = 0uy then
      sq1.active <- false

    sq1.registers.[5] <- byte (data &&& 0x40)
  )


  let write066 (_: ChannelSQ1) = IOWriter (fun _ _ ->
    ()
  )


  let write067 (_: ChannelSQ1) = IOWriter (fun _ _ ->
    ()
  )


  let clockDown sq1 =
    sq1.sweepCycles <- sq1.sweepCycles - 1

    if sq1.sweepCycles <= 0 then
      sq1.sweepCycles <- sq1.sweepCycles + sq1.sweepPeriod
      true
    else
      false


  let clockDuration sq1 =
    if sq1.duration |> Duration.clock then
      sq1.active <- false


  let clockEnvelope sq1 =
    sq1.envelope |> Envelope.clock


  let clockSweep sq1 =
    if not (clockDown sq1) || (not sq1.sweepEnable) || sq1.sweepPeriod = 0 then
      ()
    else
      let result = sq1.sweepShadow + ((sq1.sweepShadow >>> sq1.sweepShift) * sq1.sweepDelta)

      if result > 0x7ff then
        sq1.active <- false
      elif sq1.sweepShift <> 0 then
        sq1.sweepShadow <- result
        sq1.period <- frequencyToPeriod sq1.sweepShadow


  let render t sq1 =
    let mutable sum = sq1.cycles
    sq1.cycles <- sq1.cycles - t

    if sq1.active then
      if sq1.cycles >= 0 then
        ((Envelope.getOutput sq1.envelope) >>> dutyTable.[sq1.dutyForm].[sq1.dutyStep]) &&& 0xff
      else
        sum <- sum >>> dutyTable.[sq1.dutyForm].[sq1.dutyStep]

        while (sq1.cycles < 0) do
          sq1.cycles <- sq1.cycles + sq1.period
          sq1.dutyStep <- (sq1.dutyStep - 1) &&& 0x7
          sum <- sum + ((min (-sq1.cycles) sq1.period) >>> dutyTable.[sq1.dutyForm].[sq1.dutyStep])

        ((sum * (Envelope.getOutput sq1.envelope)) / t) &&& 0xff
    else
      while (sq1.cycles < 0) do
        sq1.cycles <- sq1.cycles + sq1.period
        sq1.dutyStep <- (sq1.dutyStep - 1) &&& 0x7

      0
