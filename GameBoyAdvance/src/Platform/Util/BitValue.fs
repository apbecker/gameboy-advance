namespace GameBoyAdvance.Platform.Util

type BitValue =
  | Off
  | On

[<RequireQualifiedAccess>]
module BitValue =

  let toBoolean =
    function
    | Off -> false
    | On -> true


  let ofBoolean =
    function
    | false -> Off
    | true -> On


  let toInt32 =
    function
    | Off -> 0
    | On -> 1


  let ofInt32 =
    function
    | 0 -> Off
    | _ -> On
