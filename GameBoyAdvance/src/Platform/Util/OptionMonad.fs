[<AutoOpen>]
module GameBoyAdvance.Platform.Util.OptionMonad

type OptionBuilder () =
  member __.Bind (value, binder) = Option.bind binder value
  member __.Return a : Option<_> = Some a
  member __.ReturnFrom a : Option<_> = a


let maybe = OptionBuilder ()
