namespace GameBoyAdvance

[<RequireQualifiedAccess>]
type Size =
  | Byte
  | Half
  | Word

type IOReader = IOReader of (int -> byte)

type IOWriter = IOWriter of (int -> byte -> unit)
