namespace GameBoyAdvance.PPU.Sprites

type Sprite =
  { attr0: Attr0
    attr1: Attr1
    attr2: Attr2
    w: int
    h: int
    clippingW: int
    clippingH: int }

module Sprite =

  let sizeLut = [|
    [| ( 8,  8); (16, 16); (32, 32); (64, 64) |] (* square *)
    [| (16,  8); (32,  8); (32, 16); (64, 32) |] (* wide   *)
    [| ( 8, 16); ( 8, 32); (16, 32); (32, 64) |] (* tall   *)
  |]


  let fromAttributes attribute0 attribute1 attribute2 f =
    let attr0 = Attr0.fromPrimitive(attribute0)
    let attr1 = Attr1.fromPrimitive(attribute1)
    let attr2 = Attr2.fromPrimitive(attribute2)

    if attr0.type' <> SpriteType.Undefined && attr0.mode <> SpriteMode.Undefined && attr0.shape <> 3 then
      let (w, h) = sizeLut.[attr0.shape].[attr1.size]
      let scale = if attr0.type' = SpriteType.AffineLarge then 2 else 1

      f { attr0 = attr0
          attr1 = attr1
          attr2 = attr2
          w = w
          h = h
          clippingW = w * scale
          clippingH = h * scale }
