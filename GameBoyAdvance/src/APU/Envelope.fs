namespace GameBoyAdvance.APU

type Envelope =
  { mutable upward: int
    mutable volume: int
    mutable cycles: int
    mutable period: int }

module Envelope =

  let init () =
    { upward = 0
      volume = 0
      cycles = 0
      period = 0 }


  let clock envelope =
    if envelope.cycles <> 0 && envelope.period <> 0 then
      envelope.cycles <- envelope.cycles - 1

      if envelope.cycles = 0 then
        envelope.cycles <- envelope.period

        match envelope.upward with
        | 1 when envelope.volume < 0xf -> envelope.volume <- envelope.volume + 1
        | 0 when envelope.volume > 0x0 -> envelope.volume <- envelope.volume - 1
        | _ -> ()


  let reset envelope =
    envelope.cycles <- envelope.period


  let write (data: byte) envelope =
    envelope.volume <- ((int data) >>> 4) &&& 15
    envelope.upward <- ((int data) >>> 3) &&& 1
    envelope.period <- ((int data) >>> 0) &&& 7


  let getOutput envelope =
    envelope.volume
