module GameBoyAdvance.Processors.ARM7.Mode

let [<Literal>] USR = 0x10
let [<Literal>] FIQ = 0x11
let [<Literal>] IRQ = 0x12
let [<Literal>] SVC = 0x13
let [<Literal>] ABT = 0x17
let [<Literal>] UND = 0x1b
let [<Literal>] SYS = 0x1f
