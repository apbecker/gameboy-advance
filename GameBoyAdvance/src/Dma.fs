namespace GameBoyAdvance

open System
open GameBoyAdvance.CPU
open GameBoyAdvance.Memory


type DmaType =
  | Immediate
  | VBlank
  | HBlank
  | Special


type Dma =
  { interruptType: Interrupt
    mutable controlRegister: uint16
    mutable lengthRegister: uint16
    mutable sourceRegister: int
    mutable targetRegister: int
    mutable length: uint16
    mutable target: int
    mutable targetStep: int
    mutable source: int
    mutable sourceStep: int
    mutable Pending: bool }

module Dma =

  let stepLut = [|
    1; -1; 0; 1
  |]


  let getEnabled dma =
    (dma.controlRegister &&& 0x8000us) <> 0us


  let getType dma =
    match ((int dma.controlRegister) >>> 12) &&& 3 with
    | 0 -> Immediate
    | 1 -> VBlank
    | 2 -> HBlank
    | _ -> Special


  let init (interruptType: Interrupt) =
    { interruptType = interruptType
      controlRegister = 0us
      lengthRegister = 0us
      sourceRegister = 0
      targetRegister = 0
      length = 0us
      target = 0
      targetStep = 0
      source = 0
      sourceStep = 0
      Pending = false }


  let transferInternal reader writer size count dma =
    let shift =
      match size with
      | Size.Byte -> 0
      | Size.Half -> 1
      | Size.Word -> 2

    dma.targetStep <- dma.targetStep <<< shift
    dma.sourceStep <- dma.sourceStep <<< shift

    let rec loop count =
      let data = reader size dma.source
      writer size dma.target data

      dma.target <- dma.target + dma.targetStep
      dma.source <- dma.source + dma.sourceStep

      match count - 1us with
      | 0us -> ()
      | cnt -> loop cnt

    loop count


  // #region Registers


  let readControl0 dma = IOReader (fun _ ->
    uint8 (dma.controlRegister &&& 0xe0us)
  )


  let readControl1 dma = IOReader (fun address ->
    let ctrl = uint8 (dma.controlRegister >>> 8)

    match address with
    | 0xbb -> ctrl &&& 0xf7uy
    | 0xc7 -> ctrl &&& 0xf7uy
    | 0xd3 -> ctrl &&& 0xf7uy
    | 0xdf -> ctrl &&& 0xffuy
    | _    -> raise (ArgumentOutOfRangeException())
  )


  let writeControl0 dma = IOWriter (fun _ data ->
    dma.controlRegister <- dma.controlRegister &&& 0xff00us
    dma.controlRegister <- dma.controlRegister ||| (uint16 data)
  )


  let writeControl1 dma = IOWriter (fun _ data ->
    let prev = ((int dma.controlRegister) >>> 8) &&& 0x80
    let next = (int data) &&& 0x80

    if prev < next then
      dma.length <- dma.lengthRegister
      dma.target <- dma.targetRegister
      dma.source <- dma.sourceRegister

      if (data &&& 0x30uy) = 0x00uy then
        dma.Pending <- true

    dma.controlRegister <- dma.controlRegister &&& 0x00ffus
    dma.controlRegister <- dma.controlRegister ||| ((uint16 data) <<< 8)
  )


  let writeCounter0 dma = IOWriter (fun _ data ->
    let data = uint16 data
    dma.lengthRegister <- dma.lengthRegister &&& 0xff00us
    dma.lengthRegister <- dma.lengthRegister ||| data
  )


  let writeCounter1 dma = IOWriter (fun _ data ->
    let data = uint16 data
    dma.lengthRegister <- dma.lengthRegister &&& 0x00ffus
    dma.lengthRegister <- dma.lengthRegister ||| (data <<< 8)
  )


  let writeDstAddr dma = IOWriter (fun address data ->
    match address &&& 3 with
    | 0 -> dma.targetRegister <- (dma.targetRegister &&& 0xffffff00) ||| ((int data) <<< 0)
    | 1 -> dma.targetRegister <- (dma.targetRegister &&& 0xffff00ff) ||| ((int data) <<< 8)
    | 2 -> dma.targetRegister <- (dma.targetRegister &&& 0xff00ffff) ||| ((int data) <<< 16)
    | _ -> dma.targetRegister <- (dma.targetRegister &&& 0x00ffffff) ||| ((int data) <<< 24)
  )


  let writeSrcAddr dma = IOWriter (fun address data ->
    match address &&& 3 with
    | 0 -> dma.sourceRegister <- (dma.sourceRegister &&& 0xffffff00) ||| ((int data) <<< 0)
    | 1 -> dma.sourceRegister <- (dma.sourceRegister &&& 0xffff00ff) ||| ((int data) <<< 8)
    | 2 -> dma.sourceRegister <- (dma.sourceRegister &&& 0xff00ffff) ||| ((int data) <<< 16)
    | _ -> dma.sourceRegister <- (dma.sourceRegister &&& 0x00ffffff) ||| ((int data) <<< 24)
  )


  // #endregion

  let initialize address mmio dma =
    mmio |> MMIO.wo (address + 0x0) (writeSrcAddr dma) // $40000B0 - 32 - DMA0SAD
    mmio |> MMIO.wo (address + 0x1) (writeSrcAddr dma)
    mmio |> MMIO.wo (address + 0x2) (writeSrcAddr dma)
    mmio |> MMIO.wo (address + 0x3) (writeSrcAddr dma)
    mmio |> MMIO.wo (address + 0x4) (writeDstAddr dma) // $40000B4 - 32 - DMA0DAD
    mmio |> MMIO.wo (address + 0x5) (writeDstAddr dma)
    mmio |> MMIO.wo (address + 0x6) (writeDstAddr dma)
    mmio |> MMIO.wo (address + 0x7) (writeDstAddr dma)
    mmio |> MMIO.wo (address + 0x8) (writeCounter0 dma) // $40000B8 - 16 - DMA0CNT(L)
    mmio |> MMIO.wo (address + 0x9) (writeCounter1 dma)
    mmio |> MMIO.rw (address + 0xa) (readControl0 dma) (writeControl0 dma) // $40000BA - 16 - DMA0CNT(H)
    mmio |> MMIO.rw (address + 0xb) (readControl1 dma) (writeControl1 dma)


  let transfer reader writer sendIRQ dma =
    dma.targetStep <- stepLut.[((int dma.controlRegister) >>> 5) &&& 3]
    dma.sourceStep <- stepLut.[((int dma.controlRegister) >>> 7) &&& 3]

    let mutable count = (dma.lengthRegister)
    let mutable width = if (dma.controlRegister &&& 0x0400us) <> 0us then Size.Word else Size.Half

    if getType dma = Special then
      dma.targetStep <- 0
      count <- 4us
      width <- Size.Word

    dma |> transferInternal reader writer width count

    if (dma.controlRegister &&& 0x0060us) = 0x0060us then dma.target <- dma.targetRegister
    if (dma.controlRegister &&& 0x0200us) = 0x0000us then dma.controlRegister <- dma.controlRegister &&& 0x7fffus
    if (dma.controlRegister &&& 0x0200us) = 0x0200us then dma.length <- dma.controlRegister

    if (dma.controlRegister &&& 0x4000us) <> 0us then
      sendIRQ dma.interruptType
