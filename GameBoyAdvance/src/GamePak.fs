namespace GameBoyAdvance

open System.IO
open GameBoyAdvance.Memory
open GameBoyAdvance.Platform.Util

[<RequireQualifiedAccess>]
type WaitState =
  | WS0 = 0
  | WS1 = 1
  | WS2 = 2


type GamePak =
  { buffer: uint16 []
    mask: int
    romAccess1: int []
    romAccess2: int []
    mutable ramAccess: int }

module GamePak =

  let access1Table = [| 4; 3; 2; 8 |]


  let access2Table = [|
    [| 2; 4; 8 |]
    [| 1; 1; 1 |]
  |]


  let setRamAccessTiming speedSpecifier gamePak =
    gamePak.ramAccess <- access1Table.[speedSpecifier]


  let set1stAccessTiming speedSpecifier waitState gamePak =
    let region = int waitState

    gamePak.romAccess1.[region] <- access1Table.[speedSpecifier]


  let set2ndAccessTiming speedSpecifier waitState gamePak =
    let region = int waitState

    gamePak.romAccess2.[region] <- access2Table.[speedSpecifier].[region]


  let init binary =
    let length = MathHelper.nextPowerOfTwo(Array.length binary) / sizeof<uint16>
    use stream = new MemoryStream(binary)
    use reader = new BinaryReader(stream)

    let buffer =
      seq { 1 .. length }
        |> Seq.map (fun _ -> reader.ReadUInt16 ())
        |> Seq.toArray

    let result =
      { buffer = buffer
        mask = length - 1
        romAccess1 = Array.zeroCreate 3
        romAccess2 = Array.zeroCreate 3
        ramAccess = 0 }

    result |> setRamAccessTiming 0
    result |> set1stAccessTiming 0 WaitState.WS0
    result |> set2ndAccessTiming 0 WaitState.WS0
    result |> set1stAccessTiming 0 WaitState.WS1
    result |> set2ndAccessTiming 0 WaitState.WS1
    result |> set1stAccessTiming 0 WaitState.WS2
    result |> set2ndAccessTiming 0 WaitState.WS2
    result


  let readRomHalf addClock (address: int) gamePak =
    let region = (address >>> 25) &&& 3
    addClock gamePak.romAccess2.[region]

    let compare = (address &&& 0x01ffffff) >>> 1
    if compare > gamePak.mask then
      compare
    else
      int <| gamePak.buffer.[(address >>> 1) &&& gamePak.mask]


  let readRomByte addClock (address: int) gamePak =
    match address &&& 1 with
    | 0 -> ((gamePak |> readRomHalf addClock address) >>> 0) &&& 0xff
    | _ -> ((gamePak |> readRomHalf addClock address) >>> 8) &&& 0xff


  let readRomWord addClock address gamePak =
    let lower = gamePak |> readRomHalf addClock (address &&& ~~~2)
    let upper = gamePak |> readRomHalf addClock (address |||    2)

    (upper <<< 16) ||| lower


  let readRam addClock size address gamePak =
    addClock gamePak.ramAccess
    0


  let readRom addClock size address gamePak =
    match size with
    | Size.Byte -> gamePak |> readRomByte addClock address
    | Size.Half -> gamePak |> readRomHalf addClock address
    | Size.Word -> gamePak |> readRomWord addClock address


  let writeRam addClock size address data gamePak =
    addClock gamePak.ramAccess


  let writeRom addClock (size: Size) (address: int) (data: int) (gamePak: GamePak) =
    ()


  let write204 gamePak = IOWriter (fun _ data ->
    let data = int data
    gamePak |> setRamAccessTiming ((data >>> 0) &&& 3)
    gamePak |> set1stAccessTiming ((data >>> 2) &&& 3) WaitState.WS0
    gamePak |> set2ndAccessTiming ((data >>> 4) &&& 1) WaitState.WS0
    gamePak |> set1stAccessTiming ((data >>> 5) &&& 3) WaitState.WS1
    gamePak |> set2ndAccessTiming ((data >>> 7) &&& 1) WaitState.WS1
  )


  let write205 gamePak = IOWriter (fun _ data ->
    let data = int data
    gamePak |> set1stAccessTiming ((data >>> 0) &&& 3) WaitState.WS2
    gamePak |> set2ndAccessTiming ((data >>> 2) &&& 1) WaitState.WS2
  )


  let wireUp mmio gamePak =
    mmio |> MMIO.wo 0x204 (write204 gamePak)
    mmio |> MMIO.wo 0x205 (write205 gamePak)
    // 20a - 20f
