namespace GameBoyAdvance.APU

type ChannelNOI =
  { duration: Duration
    envelope: Envelope
    registers: byte[]

    mutable active: bool
    mutable cycles: int
    mutable period: int

    output: bool[]

    mutable shift: int
    mutable value: int }

module ChannelNOI =

  open GameBoyAdvance


  let divisorTable = [|
    0x08; 0x10; 0x20; 0x30; 0x40; 0x50; 0x60; 0x70
  |]


  let init () =
    let period = divisorTable.[0] * 4 * ApuConsts.Single

    { duration = Duration.init 64
      envelope = Envelope.init ()
      registers = Array.zeroCreate 8

      active = false
      cycles = period
      period = period

      output = Array.zeroCreate 2

      shift = 8
      value = 0x6000 }


  let readReg noi = IOReader (fun address ->
    noi.registers.[address &&& 7]
  )


  let write078 noi = IOWriter (fun _ data ->
    noi.duration |> Duration.write1(data)
  )


  let write079 noi = IOWriter (fun _ data ->
    noi.envelope |> Envelope.write(data)
    noi.registers.[1] <- data
  )


  let write07A (_: ChannelNOI) = IOWriter (fun _ _ ->
    ()
  )


  let write07B (_: ChannelNOI) = IOWriter (fun _ _ ->
    ()
  )


  let write07C noi = IOWriter (fun _ data ->
    let data = int data
    noi.shift <- data &&& 0x8
    noi.period <- (divisorTable.[data &&& 0x7] <<< (data >>> 4)) * 4 * ApuConsts.Single

    noi.registers.[4] <- uint8 data
  )


  let write07D noi = IOWriter (fun _ data ->
    if data >= 0x80uy then
      noi.active <- true
      noi.cycles <- noi.period

      noi.duration |> Duration.reset
      noi.envelope |> Envelope.reset

      noi.value <- 0x4000 >>> noi.shift

    noi.duration |> Duration.write2(data)

    if (noi.registers.[1] &&& 0xf8uy) = 0uy then
      noi.active <- false

    noi.registers.[5] <- data &&& 0x40uy
  )


  let write07E (_: ChannelNOI) = IOWriter (fun _ _ ->
    ()
  )


  let write07F (_: ChannelNOI) = IOWriter (fun _ _ ->
    ()
  )


  let clockDuration noi =
    if noi.duration |> Duration.clock then
      noi.active <- false


  let clockEnvelope noi =
    noi.envelope |> Envelope.clock


  let render t noi =
    let mutable sum = noi.cycles
    noi.cycles <- noi.cycles - t

    if noi.active then
      if noi.cycles >= 0 then
        if (noi.value &&& 0x1) <> 0 then
          noi.envelope |> Envelope.getOutput
        else
          0
      else
        if (noi.value &&& 1) = 0 then
          sum <- 0

        while noi.cycles < 0 do
          noi.cycles <- noi.cycles + noi.period

          //int feedback = (((value >> 1) ^ value) & 1)
          //value = ((value >> 1) | (feedback << xor))

          if (noi.value &&& 1) <> 0 then
            noi.value <- (noi.value >>> 1) ^^^ (0x6000 >>> noi.shift)
            sum <- sum + (min (-noi.cycles) noi.period)
          else
            noi.value <- (noi.value >>> 1)

        ((sum * (Envelope.getOutput noi.envelope)) / t) &&& 0xff
    else
      while noi.cycles < 0 do
        noi.cycles <- noi.cycles + noi.period
        noi.value <-
          if (noi.value &&& 1) <> 0 then
            (noi.value >>> 1) ^^^ (0x6000 >>> noi.shift)
          else
            (noi.value >>> 1)

      0
        //int feedback = (value ^ (value >> 1)) & 0x1
        //value = ((value >> 1) | (feedback << shift))
