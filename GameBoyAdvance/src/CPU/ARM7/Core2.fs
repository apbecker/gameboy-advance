module GameBoyAdvance.Processors.ARM7.Core2

let step reader writer core =
  if core.halt then
    core.cycles <- core.cycles + 1
  else
    if core.cpsr.t = 0 then core |> ARMv4.armv4Execute reader writer
    if core.cpsr.t = 1 then core |> THUMB.thumbExecute reader writer
