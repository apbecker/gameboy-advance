namespace GameBoyAdvance.Platform

module Messaging =

  type IMessageSender = unit -> unit

  type IMessageSender<'a> = 'a -> unit
