[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.FileHelper

open System.IO
open System.Reflection


let readAllBytes path =
  let assembly = Assembly.GetEntryAssembly () in
  let location = assembly.Location in
  let folder = Path.GetDirectoryName location in
  let absolutePath = Path.Combine (folder, path) in

  File.ReadAllBytes absolutePath
