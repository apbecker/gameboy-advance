namespace GameBoyAdvance

open GameBoyAdvance.CPU

type DmaController =
  { channels: Dma [] }

module DmaController =

  let init () =
    { channels = [|
        Dma.init Interrupt.Dma0
        Dma.init Interrupt.Dma1
        Dma.init Interrupt.Dma2
        Dma.init Interrupt.Dma3
      |] }


  let wireUp mmio controller =
    controller.channels.[0] |> Dma.initialize 0x0b0 mmio
    controller.channels.[1] |> Dma.initialize 0x0bc mmio
    controller.channels.[2] |> Dma.initialize 0x0c8 mmio
    controller.channels.[3] |> Dma.initialize 0x0d4 mmio


  let vBlank ctrl =
    for channel in ctrl.channels do
      if Dma.getEnabled channel && Dma.getType channel = VBlank then
        channel.Pending <- true


  let hBlank ctrl =
    for channel in ctrl.channels do
      if Dma.getEnabled channel && Dma.getType channel = HBlank then
        channel.Pending <- true


  let transfer reader writer sendIRQ ctrl =
    for channel in ctrl.channels do
      if Dma.getEnabled channel && channel.Pending then
        channel.Pending <- false
        channel |> Dma.transfer reader writer sendIRQ
