namespace GameBoyAdvance.PPU.Backgrounds

type LinearTile =
  { name: int
    xFlip: bool
    yFlip: bool
    palette: int }

module LinearTile =

  let fromPrimitive value =
    { name    = (value &&& 0x03ff) >>> 0
      xFlip   = (value &&& 0x0400) <>  0
      yFlip   = (value &&& 0x0800) <>  0
      palette = (value &&& 0xf000) >>> 12 }
