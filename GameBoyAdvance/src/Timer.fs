namespace GameBoyAdvance

open GameBoyAdvance.CPU

type Timer =
  { interrupt: Interrupt
    mutable control: int
    mutable counter: int
    mutable refresh: int }

module Timer =

  let init interrupt =
    { interrupt = interrupt
      control = 0
      counter = 0
      refresh = 0 }
