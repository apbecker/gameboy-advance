namespace GameBoyAdvance.APU

type ChannelPCM =
  { fifo: int8 []
    mutable sz: int
    mutable rd: int
    mutable wr: int

    output: bool []

    mutable level: int
    mutable shift: int
    mutable timer: int }

module ChannelPCM =

  open GameBoyAdvance


  let init () =
    { fifo = Array.zeroCreate 32
      sz = 0
      rd = 0
      wr = 0

      output = Array.zeroCreate 2

      level = 0
      shift = 0
      timer = 0 }


  let clock requestMoreData pcm =
    if pcm.sz > 0 then
      pcm.level <- (int pcm.fifo.[pcm.rd]) <<< 1
      pcm.rd <- (pcm.rd + 1) &&& 31
      pcm.sz <- (pcm.sz - 1)

    if pcm.sz <= 16 then
      requestMoreData ()
      // if pcm.dma.Enabled && pcm.dma.Type = Dma.SPECIAL then
      //   pcm.dma.Pending <- true


  let reset pcm =
    pcm.sz <- 0
    pcm.rd <- 0
    pcm.wr <- 0


  let write pcm = IOWriter (fun _ data ->
    if pcm.sz < 32 then
      pcm.fifo.[pcm.wr] <- int8 data
      pcm.wr <- (pcm.wr + 1) &&& 31
      pcm.sz <- (pcm.sz + 1)
  )
