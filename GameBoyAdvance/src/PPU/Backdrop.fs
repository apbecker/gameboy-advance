module GameBoyAdvance.PPU.Backdrop

let init =
  { new IPixelSource with
      member __.GetColorAddress _ = 0x5000000
      member __.GetMask () = 0x20
      member __.GetPriority _ = 3
      member __.IsSemiTransparent _ = false }
