module GameBoyAdvance.APU.ApuConsts

let [<Literal>] Frequency = 16777216
let [<Literal>] Single = 48000
