namespace GameBoyAdvance.Platform.Util

[<Struct>]
type BinaryUnit =
  private
    | Byte of Byte:int
    | KiB of KiB:int
    | MiB of MiB:int

[<RequireQualifiedAccess>]
module BinaryUnit =

  let byte n =
    Byte n


  let kib n =
    KiB n


  let mib n =
    MiB n


  let byteSize unit =
    match unit with
    | Byte n -> n
    | KiB  n -> n <<< 10
    | MiB  n -> n <<< 20
