[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.String

let fold f init s =
  let rec loop index value =
    if index = String.length s then
      value
    else
      let ch = s.[index] in
      loop (index + 1) (f value ch)
  in
  loop 0 init
