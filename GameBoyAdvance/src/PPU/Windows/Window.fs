namespace GameBoyAdvance.PPU.Windows

type Window =
  { mutable enabled: bool
    mutable x1: int
    mutable x2: int
    mutable y1: int
    mutable y2: int
    mutable insideFlags: WindowFlags }

module Window =

  let init () =
    { enabled = false
      x1 = 0
      x2 = 0
      y1 = 0
      y2 = 0
      insideFlags = WindowFlags.None }


  let rangeCheck min max value =
    if min <= max then
      value >= min && value < max
    else
      value >= min || value < max


  let inRange x y window =
    window.enabled &&
    (rangeCheck window.x1 window.x2 x) &&
    (rangeCheck window.y1 window.y2 y)
