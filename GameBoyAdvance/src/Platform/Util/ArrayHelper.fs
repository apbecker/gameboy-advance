[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.ArrayHelper

open System


let create1 factory length =
  factory
    |> Seq.init length
    |> Seq.toArray
  

let fill1 length value =
  create1 (fun _ -> value) length


let create2 factory length1 length2 =
  length1 |> create1 (fun i ->
  length2 |> create1 (fun j -> factory i j))


let fill2 length1 length2 value =
  create2 (fun _ _ -> value) length1 length2


let bitMapped decoder callback =
  let length = (decoder (-1)) + 1 in
  let result = Array.zeroCreate<_> length in

  let mapper =
    BitString.bitMap decoder result

  callback mapper

  result
