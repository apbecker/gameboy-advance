namespace GameBoyAdvance

open GameBoyAdvance.Memory
open GameBoyAdvance.Platform.Input

type Pad =
  { input: InputBackend
    mutable data: uint16
    mutable mask: uint16 }

module Pad =

  let init () =
    let pad = InputBackend (0, 10)
    pad.Map 0 "A"          // 0 - Button A (0=Pressed, 1=Released)
    pad.Map 1 "X"          // 1 - Button B (etc.)
    pad.Map 2 "Back"       // 2 - Select   (etc.)
    pad.Map 3 "Menu"       // 3 - Start    (etc.)
    pad.Map 4 "DPad-R"     // 4 - Right    (etc.)
    pad.Map 5 "DPad-L"     // 5 - Left     (etc.)
    pad.Map 6 "DPad-U"     // 6 - Up       (etc.)
    pad.Map 7 "DPad-D"     // 7 - Down     (etc.)
    pad.Map 8 "R-Shoulder" // 8 - Button R (etc.)
    pad.Map 9 "L-Shoulder" // 9 - Button L (etc.)

    { input = pad
      data = 0us
      mask = 0us }


  let frame pad =
    pad.input.Update ()
    pad.data <- 0us

    if pad.input.Pressed 0 then pad.data <- pad.data ||| 0x0001us
    if pad.input.Pressed 1 then pad.data <- pad.data ||| 0x0002us
    if pad.input.Pressed 2 then pad.data <- pad.data ||| 0x0004us
    if pad.input.Pressed 3 then pad.data <- pad.data ||| 0x0008us
    if pad.input.Pressed 4 then pad.data <- pad.data ||| 0x0010us
    if pad.input.Pressed 5 then pad.data <- pad.data ||| 0x0020us
    if pad.input.Pressed 6 then pad.data <- pad.data ||| 0x0040us
    if pad.input.Pressed 7 then pad.data <- pad.data ||| 0x0080us
    if pad.input.Pressed 8 then pad.data <- pad.data ||| 0x0100us
    if pad.input.Pressed 9 then pad.data <- pad.data ||| 0x0200us

    if (pad.mask &&& 0x4000us) <> 0us then
      if (pad.mask &&& 0x8000us) <> 0us then
        ()
        // if ((data.w & mask.w) != 0)
        // {
        //     cpu.Interrupt(Cpu.Source.JOYPAD)
        // }
      else
        ()
        // if ((data.w & mask.w) == mask.w)
        // {
        //     cpu.Interrupt(Cpu.Source.JOYPAD)
        // }

    pad.data <- pad.data ^^^ 0x3ffus


  let private read130 pad = IOReader (fun _ ->
    (byte)(pad.data >>> 0)
  )


  let private read131 pad = IOReader (fun _ ->
    (byte)(pad.data >>> 8)
  )


  let private read132 pad = IOReader (fun _ ->
    (byte)(pad.mask >>> 0)
  )


  let private read133 pad = IOReader (fun _ ->
    (byte)(pad.mask >>> 8)
  )


  let private write132 pad = IOWriter (fun _ data ->
    pad.mask <- pad.mask &&& 0xff00us
    pad.mask <- pad.mask ||| (uint16 data)
  )


  let private write133 pad = IOWriter (fun _ data ->
    pad.mask <- pad.mask &&& 0x00ffus
    pad.mask <- pad.mask ||| ((uint16 data) <<< 8)
  )


  let wireUp mmio pad =
    mmio |> MMIO.ro 0x130 (read130 pad)
    mmio |> MMIO.ro 0x131 (read131 pad)
    mmio |> MMIO.rw 0x132 (read132 pad) (write132 pad)
    mmio |> MMIO.rw 0x133 (read133 pad) (write133 pad)
