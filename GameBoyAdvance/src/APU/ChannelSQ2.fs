namespace GameBoyAdvance.APU

type ChannelSQ2 =
  { duration: Duration
    envelope: Envelope
    registers: byte[]

    output: bool[]

    mutable active: bool
    mutable frequency: int
    mutable cycles: int
    mutable period: int

    mutable dutyForm: int
    mutable dutyStep: int }

module ChannelSQ2 =    

  open System
  open GameBoyAdvance


  let dutyTable = [|
    [| 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x00 |]
    [| 0x00; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x1f; 0x00 |]
    [| 0x00; 0x1f; 0x1f; 0x1f; 0x1f; 0x00; 0x00; 0x00 |]
    [| 0x1f; 0x00; 0x00; 0x00; 0x00; 0x00; 0x00; 0x1f |]
  |]


  let frequencyToPeriod (f: int) =
    (2048 - f) * 16 * ApuConsts.Single


  let init () =
    let period = frequencyToPeriod 0

    { duration = Duration.init 64
      envelope = Envelope.init ()
      registers = Array.zeroCreate 8

      output = Array.zeroCreate 2

      active = false
      frequency = 0
      cycles = period
      period = period

      dutyForm = 0
      dutyStep = 7 }


  let readReg sq2 = IOReader (fun address ->
    sq2.registers.[address &&& 7]
  )


  let write068 sq2 = IOWriter (fun _ data ->
    sq2.dutyForm <- (int data) >>> 6
    sq2.duration |> Duration.write1(data)

    sq2.registers.[0] <- data &&& 0xc0uy
  )


  let write069 sq2 = IOWriter (fun _ data ->
    sq2.envelope |> Envelope.write(data)
    sq2.registers.[1] <- data
  )


  let write06A (_: ChannelSQ2) = IOWriter (fun _ data ->
    ()
  )


  let write06B (_: ChannelSQ2) = IOWriter (fun _ data ->
    ()
  )


  let write06C sq2 = IOWriter (fun _ data ->
    let data = int data
    sq2.frequency <- (sq2.frequency &&& 0x700) ||| ((data <<< 0) &&& 0x0FF)
    sq2.period <- frequencyToPeriod(sq2.frequency)
  )


  let write06D sq2 = IOWriter (fun _ data ->
    let data = int data
    sq2.frequency <- (sq2.frequency &&& 0x0FF) ||| ((data <<< 8) &&& 0x700)
    sq2.period <- frequencyToPeriod(sq2.frequency)

    if (data &&& 0x80) <> 0 then
      sq2.active <- true
      sq2.cycles <- sq2.period

      sq2.duration |> Duration.reset
      sq2.envelope |> Envelope.reset

      sq2.dutyStep <- 7

    sq2.duration |> Duration.write2(uint8 data)

    if (sq2.registers.[1] &&& 0xf8uy) = 0uy then
      sq2.active <- false

    sq2.registers.[5] <- uint8 (data &&& 0x40)
  )


  let write06E (_: ChannelSQ2) = IOWriter (fun _ _ ->
    ()
  )


  let write06F (_: ChannelSQ2) = IOWriter (fun _ _ ->
    ()
  )


  let clockDuration sq2 =
    if sq2.duration |> Duration.clock then
      sq2.active <- false


  let clockEnvelope sq2 =
    sq2.envelope |> Envelope.clock


  let render (t: int) sq2 =
    let mutable sum = sq2.cycles
    sq2.cycles <- sq2.cycles - t

    if sq2.active then
      if sq2.cycles >= 0 then
        ((Envelope.getOutput sq2.envelope) >>> dutyTable.[sq2.dutyForm].[sq2.dutyStep])
      else
        sum <- sum >>> dutyTable.[sq2.dutyForm].[sq2.dutyStep]

        while sq2.cycles < 0 do
          sq2.cycles <- sq2.cycles + sq2.period
          sq2.dutyStep <- (sq2.dutyStep - 1) &&& 0x7
          sum <- sum + (Math.Min(-sq2.cycles, sq2.period) >>> dutyTable.[sq2.dutyForm].[sq2.dutyStep])

        ((sum * (Envelope.getOutput sq2.envelope)) / t) &&& 0xff
    else
      while sq2.cycles < 0 do
        sq2.cycles <- sq2.cycles + sq2.period
        sq2.dutyStep <- (sq2.dutyStep - 1) &&& 0x7

      0
