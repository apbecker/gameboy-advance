namespace GameBoyAdvance.CPU

open GameBoyAdvance
open GameBoyAdvance.Processors.ARM7

type Cpu =
  { core: Core
    mutable ief: uint16
    mutable irf: uint16
    mutable ime: bool }

module Cpu =

  let write writer size address data =
    let data =
      match size with
      | Size.Byte -> (data &&& 0x000000ff) * 0x01010101
      | Size.Half -> (data &&& 0x0000ffff) * 0x00010001
      | Size.Word -> (data)

    writer size address data


  let init () =
    { core = Core.Create ()
      ief = 0us
      irf = 0us
      ime = false }


  let update reader writer runDMA this =
    this.core.cycles <- 0
    this.core.interrupt <- this.ime && ((this.ief &&& this.irf) <> 0us)

    Core2.step reader (write writer) this.core

    runDMA ()

    this.core.cycles


  let irq cpu (e: Interrupt) =
    cpu.irf <- cpu.irf ||| uint16 e


  let addClock cpu amount =
    cpu.core.cycles <- cpu.core.cycles + amount


  let getProgramCursor cpu =
    cpu.core.GetProgramCursor()


  let read200 cpu address =
    byte (cpu.ief >>> 0)


  let read201 cpu address =
    byte (cpu.ief >>> 8)


  let read202 cpu address =
    byte (cpu.irf >>> 0)


  let read203 cpu address =
    byte (cpu.irf >>> 8)


  let write200 cpu address (data: uint8) =
    cpu.ief <- cpu.ief &&& 0xff00us
    cpu.ief <- cpu.ief ||| (uint16 data)


  let write201 cpu address (data: uint8) =
    cpu.ief <- cpu.ief &&& 0x00ffus
    cpu.ief <- cpu.ief ||| (uint16 (data <<< 8))


  let write202 cpu address (data: uint8) =
    cpu.irf <- cpu.irf &&& (~~~((uint16 data) <<< 0))


  let write203 cpu address (data: uint8) =
    cpu.irf <- cpu.irf &&& (~~~((uint16 data) <<< 8))


  let read208 cpu address =
    byte (if cpu.ime then 1 else 0)


  let read209 cpu address =
    0uy


  let write208 cpu address data =
    cpu.ime <- (data &&& 1uy) <> 0uy


  let write209 cpu address data =
    ()


  let wireUp mmio cpu =
    mmio 0x200 (read200 cpu) (write200 cpu)
    mmio 0x201 (read201 cpu) (write201 cpu)
    mmio 0x202 (read202 cpu) (write202 cpu)
    mmio 0x203 (read203 cpu) (write203 cpu)
    mmio 0x208 (read208 cpu) (write208 cpu)
    mmio 0x209 (read209 cpu) (write209 cpu)
