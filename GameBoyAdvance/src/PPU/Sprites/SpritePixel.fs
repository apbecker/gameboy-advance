namespace GameBoyAdvance.PPU.Sprites

type SpritePixel =
  { mutable opaque: bool
    mutable semiTransparent: bool
    mutable colorAddress: int
    mutable priority: int }
