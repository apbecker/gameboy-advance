[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.MathHelper

let rec greatestCommonFactor a b =
  match b with
  | 0 -> a
  | b -> greatestCommonFactor b (a % b)


let reduce a b =
  let gcf = greatestCommonFactor a b in

  ( a / gcf
  , b / gcf )


let signExtend bits number =
  let mask = (1 <<< bits) - 1 in
  let sign = 1 <<< (bits - 1) in

  ((number &&& mask) ^^^ sign) - sign


let nextPowerOfTwo number =
  let copyBits x n =
    n ||| (n >>> x)

  let addOne n = n + 1
  let subOne n = n - 1

  number
    |> subOne
    |> copyBits 1
    |> copyBits 2
    |> copyBits 4
    |> copyBits 8
    |> copyBits 16
    |> addOne
