namespace GameBoyAdvance.PPU.Sprites

type SpriteType =
  | Linear = 0
  | Affine = 1
  | Undefined = 2
  | AffineLarge = 3

type SpriteMode =
  | Normal = 0
  | SemiTransparent = 1
  | Window = 2
  | Undefined = 3

[<Struct>]
type Attr0 =
  { y: int
    type': SpriteType
    mode: SpriteMode
    mosaic: bool
    depth: bool
    shape: int }

module Attr0 =

  let fromPrimitive value =
    { y      = ((value &&& 0x00ff) >>>  0)
      type'  = ((value &&& 0x0300) >>>  8) |> enum<SpriteType>
      mode   = ((value &&& 0x0c00) >>> 10) |> enum<SpriteMode>
      mosaic = ((value &&& 0x1000) <>   0)
      depth  = ((value &&& 0x2000) <>   0)
      shape  = ((value &&& 0xc000) >>> 14) }
