module GameBoyAdvance.Processors.ARM7.ARMv4

open GameBoyAdvance
open GameBoyAdvance.Platform.Util

type ARMv4Op =
  | ARMv4Op of (ReadFunction -> WriteFunction -> Core -> unit)

module ARMv4Op =

  let call reader writer core (ARMv4Op f) =
    f reader writer core


let getOperand2ConstantShift core =
  let shift = (core.code >>> 7) &&& 31
  let value = core.Get (core.code &&& 15) 0

  match (core.code >>> 5) &&& 3 with
  | 0 -> if shift <> 0 then core.LSL value shift else core.LSL value  0
  | 1 -> if shift <> 0 then core.LSR value shift else core.LSR value 32
  | 2 -> if shift <> 0 then core.ASR value shift else core.ASR value 32
  | _ -> if shift <> 0 then core.ROR value shift else core.RRX value


let getOperand2RegisterShift core =
  let rs = (core.code >>> 8) &&& 15
  let rm = (core.code >>> 0) &&& 15
  let shift = (core.Get rs 0) &&& 255
  let value = (core.Get rm 4)

  core.cycles <- core.cycles + 1

  match (core.code >>> 5) &&& 3 with
  | 0 -> core.LSL value shift
  | 1 -> core.LSR value shift
  | 2 -> core.ASR value shift
  | _ -> core.ROR value shift


//#region Opcodes


let opSWI = ARMv4Op (fun reader writer core ->
  core.Isr Mode.SVC Vector.SWI
)


let opUND = ARMv4Op (fun reader writer core ->
  core.Isr Mode.UND Vector.UND
)


let opBranch = ARMv4Op (fun reader writer core ->
  let link = (core.code >>> 24) &&& 1
  if (link = 1) then
    core.Set 14 (core.pc.value - 4)
    core.Set 15 (core.pc.value + ((MathHelper.signExtend 24 core.code) <<< 2))
  else
    core.Set 15 (core.pc.value + ((MathHelper.signExtend 24 core.code) <<< 2))
)


let opBranchExchange = ARMv4Op (fun reader writer core ->
  let rm = core.Get (core.code &&& 15) 0
  core.cpsr.t <- rm &&& 1

  core.Set 15 (rm &&& (~~~1))
)


let opDataProcessing value core =
  let o = (core.code >>> 21) &&& 15
  let n = (core.code >>> 16) &&& 15
  let d = (core.code >>> 12) &&& 15

  let rn = core.Get n 0

  match o with
  | 0x0 -> (* AND *) core.Set d (core.Mov (rn &&& value))
  | 0x1 -> (* EOR *) core.Set d (core.Mov (rn ^^^ value))
  | 0x2 -> (* SUB *) core.Set d (core.Sub rn value)
  | 0x3 -> (* RSB *) core.Set d (core.Sub value rn)
  | 0x4 -> (* ADD *) core.Set d (core.Add rn value)
  | 0x5 -> (* ADC *) core.Set d (core.AddCarry rn value core.cpsr.c)
  | 0x6 -> (* SBC *) core.Set d (core.SubCarry rn value core.cpsr.c)
  | 0x7 -> (* RSC *) core.Set d (core.SubCarry value rn core.cpsr.c)
  | 0x8 -> (* TST *) core.Mov (rn &&& value) |> ignore
  | 0x9 -> (* TEQ *) core.Mov (rn ^^^ value) |> ignore
  | 0xa -> (* CMP *) core.Sub rn value |> ignore
  | 0xb -> (* CMN *) core.Add rn value |> ignore
  | 0xc -> (* ORR *) core.Set d (core.Mov(rn ||| value))
  | 0xd -> (* MOV *) core.Set d (core.Mov(value))
  | 0xe -> (* BIC *) core.Set d (core.Mov(rn &&& (~~~value)))
  | 0xf -> (* MVN *) core.Set d (core.Mov(~~~value))
  | _ -> ()

  let s = (core.code >>> 20) &&& 1
  if s = 1 && d = 15 && core.spsr.IsSome then
    core.MoveSPSRToCPSR()


let opDataProcessingConstant = ARMv4Op (fun reader writer core ->
  let shift = (core.code >>> 7) &&& 30
  let value = (core.code >>> 0) &&& 255

  core |> opDataProcessing (core.ROR value shift)
)


let opDataProcessingConstantShift = ARMv4Op (fun reader writer core ->
  let value = getOperand2ConstantShift core

  core |> opDataProcessing value
)


let opDataProcessingRegisterShift = ARMv4Op (fun reader writer core ->
  let value = getOperand2RegisterShift core

  core |> opDataProcessing value
)


let opDataTransfer reader writer offset core =
  let p = (core.code >>> 24) &&& 1
  let u = (core.code >>> 23) &&& 1
  let b = (core.code >>> 22) &&& 1
  let w = (core.code >>> 21) &&& 1
  let l = (core.code >>> 20) &&& 1
  let n = (core.code >>> 16) &&& 15
  let d = (core.code >>> 12) &&& 15

  let offset =
    if (u = 0) then (-offset) else offset

  let rn = core.Get n 0

  let address =
    if p = 1 then
      rn + offset
    else
      rn

  if l = 1 then
    let rd =
      if b = 1 then
        core.ReadByte reader address
      else
        core.ReadWord reader address

    if (p = 0 || w = 1) then
      core.Set n (rn + offset)
      core.Set d rd
    else
      core.Set d rd
  else
    let rd = core.Get d 4

    core.Write writer (if b = 1 then Size.Byte else Size.Word) address rd

    if (p = 0 || w = 1) then
      core.Set n (rn + offset)


let opDataTransferConstantOffset = ARMv4Op (fun reader writer core ->
  let offset = core.code &&& 0xfff

  core |> opDataTransfer reader writer offset
)


let opDataTransferRegisterOffset = ARMv4Op (fun reader writer core ->
  let offset = getOperand2ConstantShift core

  core |> opDataTransfer reader writer offset
)


let opDataTransfer2 reader writer offset core =
  let p = (core.code >>> 24) &&& 1
  let u = (core.code >>> 23) &&& 1
  let w = (core.code >>> 21) &&& 1
  let l = (core.code >>> 20) &&& 1
  let n = (core.code >>> 16) &&& 15
  let d = (core.code >>> 12) &&& 15

  let offset =
    if (u = 0) then (-offset) else offset

  let rn = core.Get n 0

  let address =
    if p = 1 then
      rn + offset
    else
      rn

  if (l = 1) then
    let mutable rd = 0

    match (core.code >>> 5) &&& 3 with
    | 0 -> (* Reserved *) opUND |> ARMv4Op.call reader writer core
    | 1 -> (* LDRH     *) rd <- core.ReadHalf reader address
    | 2 -> (* LDRSB    *) rd <- core.ReadByteSignExtended reader address
    | _ -> (* LDRSH    *) rd <- core.ReadHalfSignExtended reader address

    if (p = 0 || w = 1) then
      core.Set n (rn + offset)
      core.Set d rd
    else
      core.Set d rd
  else
    let rd = core.Get d 4

    match (core.code >>> 5) &&& 3 with
    | 0 -> opUND |> ARMv4Op.call reader writer core // Reserved
    | 1 -> core.Write writer Size.Half address (rd &&& 0xffff) // STRH - TODO: store half
    | 2 -> opUND |> ARMv4Op.call reader writer core // Reserved
    | _ -> opUND |> ARMv4Op.call reader writer core // Reserved

    if (p = 0 || w = 1) then
      core.Set n (rn + offset)


let opDataTransfer2ConstantOffset = ARMv4Op (fun reader writer core ->
  let upper = (core.code >>> 8) &&& 15
  let lower = (core.code >>> 0) &&& 15

  core |> opDataTransfer2 reader writer ((upper <<< 4) + lower)
)


let opDataTransfer2RegisterOffset = ARMv4Op (fun reader writer core ->
  let offset = core.Get (core.code &&& 15) 0

  core |> opDataTransfer2 reader writer offset
)


let opLdm reader n address last (w: bool) (s: bool) currentMode (core: Core) =
  if s then
    core.ChangeRegisters(Mode.USR)

  let mutable address = address

  for i in 0 .. 15 do
    if ((core.code &&& (1 <<< i)) <> 0) then
      if (w) then
        core.Set n last

      core.Set i (core.Read reader Size.Word (address &&& (~~~3)))
      address <- address + 4

  if (s) then
    core.ChangeRegisters(currentMode)

  if (s && (core.code &&& 0x8000) <> 0) then
    core.MoveSPSRToCPSR()


let opStm writer n address last (w: bool) (s: bool) currentMode (core: Core) =
  if (s) then
    core.ChangeRegisters(Mode.USR)

  let mutable address = address

  for i in 0 .. 15 do
    if ((core.code &&& (1 <<< i)) <> 0) then
      core.Write writer Size.Word (address &&& (~~~3)) (core.Get i 0)
      address <- address + 4

      if w then
        core.Set n last

  if s then
    core.ChangeRegisters(currentMode)


let opDataTransferMultiple = ARMv4Op (fun reader writer core ->
  let p = (core.code >>> 24) &&& 1
  let u = (core.code >>> 23) &&& 1
  let s = (core.code >>> 22) &&& 1
  let w = (core.code >>> 21) &&& 1
  let l = (core.code >>> 20) &&& 1
  let n = (core.code >>> 16) &&& 15

  let bits = int <| Bit.countSetBits(core.code &&& 0xffff)

  let rn = core.Get n 0

  let address =
    if u = 1 then
      if p = 1 then
        rn + 4
      else
        rn + 0
    else
      if p = 1 then
        rn + 0 - (bits * 4)
      else
        rn + 4 - (bits * 4)

  let last =
    if u = 1 then
      rn + (bits * 4)
    else
      rn - (bits * 4)

  match l with
  | 0 -> core |> opStm writer n address last (w <> 0) (s <> 0) core.cpsr.m
  | _ -> core |> opLdm reader n address last (w <> 0) (s <> 0) core.cpsr.m
)


let opMoveStatusToRegister = ARMv4Op (fun reader writer core ->
  let p = (core.code >>> 22) &&& 1
  let d = (core.code >>> 12) &&& 15

  if (p = 0) then
    core.Set d (core.cpsr |> Flags.save)
  else
    core.spsr
      |> Option.map Flags.save
      |> Option.iter (core.Set d)
)


let opMoveToStatus value core =
  let p = (core.code >>> 22) &&& 1
  let f = (core.code >>> 19) &&& 1
  let c = (core.code >>> 16) &&& 1

  let psr =
    if p = 0 then
      core.cpsr
    else
      core.spsr |> Option.get

  if (f = 1) then
    psr.n <- (value >>> 31) &&& 1
    psr.z <- (value >>> 30) &&& 1
    psr.c <- (value >>> 29) &&& 1
    psr.v <- (value >>> 28) &&& 1

  if (c = 1 && core.cpsr.m <> Mode.USR) then
    if (p = 0) then
      core.ChangeRegisters(value &&& 31)

    psr.i <- (value >>> 7) &&& 1
    psr.f <- (value >>> 6) &&& 1
    psr.t <- (value >>> 5) &&& 1
    psr.m <- (value >>> 0) &&& 31


let opMoveRegisterToStatus = ARMv4Op (fun reader writer core ->
  let value = core.Get (core.code &&& 15) 0

  core |> opMoveToStatus(value)
)


let opMoveConstantToStatus = ARMv4Op (fun reader writer core ->
  let value = (core.code >>> 0) &&& 255
  let shift = (core.code >>> 7) &&& 30

  core |> opMoveToStatus (core.ROR value shift)
)


let opMultiply32 = ARMv4Op (fun reader writer core ->
  let a = (core.code >>> 21) &&& 1
  let d = (core.code >>> 16) &&& 15
  let n = (core.code >>> 12) &&& 15
  let s = (core.code >>> 8) &&& 15
  let m = (core.code >>> 0) &&& 15

  let rm = core.Get m 0
  let rs = core.Get s 0
  let rn = if a = 1 then core.Get n 0 else 0

  core.Set d (core.Mul rm rs rn)
)


let opMultiply64 = ARMv4Op (fun reader writer core ->
  let signExtend = (core.code >>> 22) &&& 1
  let accumulate = (core.code >>> 21) &&& 1
  let h = (core.code >>> 16) &&& 15
  let l = (core.code >>> 12) &&& 15
  let s = (core.code >>> 8) &&& 15
  let m = (core.code >>> 0) &&& 15

  let sign = 0x80000000L
  let rm = int64 <| core.Get m 0
  let rs = int64 <| core.Get s 0
  let rh = int64 <| core.Get h 0
  let rl = int64 <| core.Get l 0

  let rm = if signExtend = 0 then rm else ((rm ^^^ sign) - sign)
  let rs = if signExtend = 0 then rs else ((rs ^^^ sign) - sign)
  let rh = if accumulate = 1 then rh else 0L
  let rl = if accumulate = 1 then rl else 0L

  let rd = (rm * rs) + (rh <<< 32) + rl

  core.Set h (int (rd >>> 32))
  core.Set l (int (rd))

  let save = (core.code >>> 20) &&& 1
  if (save = 1) then
    core.cpsr.n <- (int)(rd >>> 63) &&& 1
    core.cpsr.z <- if rd = 0L then 1 else 0
)


let opSwap = ARMv4Op (fun reader writer core ->
  let m = (core.code >>>  0) &&& 15
  let d = (core.code >>> 12) &&& 15
  let n = (core.code >>> 16) &&& 15

  let rm = core.Get m 0
  let rn = core.Get n 0

  match (core.code >>> 22) &&& 1 with
  | 0 ->
    let tmp = core.ReadWord reader rn
    core.Write writer Size.Word rn rm
    core.Set d tmp

  | _ ->
    let tmp = core.ReadByte reader rn
    core.Write writer Size.Byte rn rm
    core.Set d tmp
)


//#endregion


let armv4Decode (code: int) =
  ((code >>> 16) &&& 0xff0) ||| ((code >>> 4) &&& 0x00f)


let armv4Table =
  ArrayHelper.bitMapped armv4Decode (fun map ->
    map "---- ---- ---- ---- ---- ---- ---- ----" opUND
    map "---- 000- ---- ---- ---- ---- ---0 ----" opDataProcessingConstantShift
    map "---- 000- ---- ---- ---- ---- 0--1 ----" opDataProcessingRegisterShift
    map "---- 001- ---- ---- ---- ---- ---- ----" opDataProcessingConstant
    map "---- 010- ---- ---- ---- ---- ---- ----" opDataTransferConstantOffset
    map "---- 011- ---- ---- ---- ---- ---0 ----" opDataTransferRegisterOffset
    map "---- 100- ---- ---- ---- ---- ---- ----" opDataTransferMultiple
    map "---- 101- ---- ---- ---- ---- ---- ----" opBranch
    map "---- 1111 ---- ---- ---- ---- ---- ----" opSWI
    
    map "---- 0000 -00- ---- ---- 0000 1--1 ----" opDataTransfer2RegisterOffset
    map "---- 0001 -0-- ---- ---- 0000 1--1 ----" opDataTransfer2RegisterOffset
    map "---- 0000 -10- ---- ---- ---- 1--1 ----" opDataTransfer2ConstantOffset
    map "---- 0001 -1-- ---- ---- ---- 1--1 ----" opDataTransfer2ConstantOffset
    
    map "---- 0000 00-- ---- ---- ---- 1001 ----" opMultiply32
    map "---- 0000 1--- ---- ---- ---- 1001 ----" opMultiply64
    map "---- 0001 0-00 ---- ---- 0000 1001 ----" opSwap
    map "---- 0001 0010 ---- ---- ---- 0001 ----" opBranchExchange
    
    map "---- 0001 0-00 ---- ---- ---- 0000 ----" opMoveStatusToRegister
    map "---- 0001 0-10 ---- ---- ---- 0000 ----" opMoveRegisterToStatus
    map "---- 0011 0-10 ---- ---- ---- 0000 ----" opMoveConstantToStatus
  )


let armv4Step reader core =
  core.pc.value <- core.pc.value + 4

  core.pipeline.execute <- core.pipeline.decode
  core.pipeline.decode <- core.pipeline.fetch
  core.pipeline.fetch <- reader Size.Word (core.pc.value &&& (~~~3))


let armv4Execute reader writer core =
  if core.pipeline.refresh then
    core.pipeline.refresh <- false
    core.pipeline.fetch <- reader Size.Word (core.pc.value &&& (~~~3))

    core |> armv4Step reader

  core |> armv4Step reader

  if core.interrupt && core.cpsr.i = 0 then // irq after pipeline initialized in correct mode
    core.Isr Mode.IRQ Vector.IRQ
  else
    core.code <- core.pipeline.execute

    if core.GetCondition (core.code >>> 28) then
      armv4Table.[armv4Decode core.code] |> ARMv4Op.call reader writer core
