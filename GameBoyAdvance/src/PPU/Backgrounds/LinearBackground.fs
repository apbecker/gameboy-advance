namespace GameBoyAdvance.PPU.Backgrounds

open GameBoyAdvance
open GameBoyAdvance.PPU

type LinearBackground =
  { mutable scx: int
    mutable scy: int

    mutable depth: bool
    mutable mosaic: bool
    mutable wrap: bool
    mutable colorBase: int
    mutable indexBase: int
    mutable screenSize: int

    pixels: BackgroundPixel[]

    mutable enable: bool
    mutable index: int
    mutable mask: int
    mutable priority: int }

  interface IPixelSource with
    member bg.GetColorAddress x = bg.pixels.[x].colorAddress
    member bg.GetPriority x = bg.priority
    member bg.GetMask () = bg.mask
    member bg.IsSemiTransparent x = false


module LinearBackground =

  let init index =
    { scx = 0
      scy = 0

      depth = false
      mosaic = false
      wrap = false
      colorBase = 0
      indexBase = 0
      screenSize = 0

      pixels = Array.zeroCreate 240

      enable = false
      index = index
      mask = 1 <<< index
      priority = 0 }


  let readControl0 bg (address: int) =
    uint8 (
      bg.priority |||
      (bg.colorBase >>> 12) |||
      (if bg.mosaic then 0x40 else 0) |||
      (if bg.depth then 0x80 else 0)
    )


  let readControl1 bg (address: int) =
    uint8 (
      (bg.indexBase >>> 11) |||
      (if bg.wrap then 0x20 else 0) |||
      (bg.screenSize <<< 6)
    )


  let writeControl0 bg (address: int) (data: byte) =
    let data = int data
    bg.priority <- (data &&& 0x03)
    bg.colorBase <- (data &&& 0x0c) <<< 12
    bg.mosaic <- (data &&& 0x40) <> 0
    bg.depth <- (data &&& 0x80) <> 0


  let writeControl1 bg (address: int) (data: byte) =
    let data = int data
    bg.indexBase <- (data &&& 0x1f) <<< 11
    bg.wrap <- (data &&& 0x20) <> 0 && (bg.index = 2 || bg.index = 3)
    bg.screenSize <- (data &&& 0xc0) >>> 6


  let writeScrollX bg (address: int) (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    bg.scx <- bg.scx &&& ~~~(0xff <<< shift)
    bg.scx <- bg.scx |||    (data <<< shift)


  let writeScrollY bg (address: int) (data: byte) =
    let shift = (address &&& 1) * 8
    let data = int data

    bg.scy <- bg.scy &&& ~~~(0xff <<< shift)
    bg.scy <- bg.scy |||    (data <<< shift)


  let getScx bg = bg.scx &&& 0x1ff


  let getScy bg = bg.scy &&& 0x1ff


  let readVram8 reader address =
    reader Size.Byte (Constants.VramBase + address)


  let readVram16 reader address =
    reader Size.Half (Constants.VramBase + address * 2)


  let outputPixel bg i palette colorIndex =
    if colorIndex <> 0 then
      bg.pixels.[i].opaque <- true
      bg.pixels.[i].colorAddress <- Constants.PramBase + ((palette + colorIndex) * 2)
    else
      bg.pixels.[i].opaque <- false
      bg.pixels.[i].colorAddress <- Constants.PramBase


  let outputPixel4Bpp reader bg i address palette x =
    let colorIndex = readVram8 reader (address)

    let colorIndex =
      if (x &&& 1) = 0 then
        (colorIndex >>> 0) &&& 15
      else
        (colorIndex >>> 4) &&& 15

    outputPixel bg i palette colorIndex


  let outputPixel8Bpp reader bg i address =
    let colorIndex = readVram8 reader address

    outputPixel bg i 0 colorIndex


  let renderLinear reader bg (vclock: int) =
    if not bg.enable then
      ()
    else

      let xMask = (bg.screenSize &&& 0x01) <<< 8
      let yMask = (bg.screenSize &&& 0x02) <<< 7

      let indexBase = bg.indexBase >>> 1
      let mutable xScroll = getScx bg
      let yScroll = (getScy bg) + vclock

      let mutable baseAddr = indexBase + ((yScroll &&& 0xf8) <<< 2)

      let mutable xToggle = 0
      let mutable yToggle = 0

      match bg.screenSize with
      | 0 -> xToggle <- 0x000; yToggle <- 0x000 // 32x32
      | 1 -> xToggle <- 0x400; yToggle <- 0x000 // 64x32
      | 2 -> xToggle <- 0x000; yToggle <- 0x400 // 32x64
      | _ -> xToggle <- 0x400; yToggle <- 0x800 // 64x64

      if (yScroll &&& yMask) <> 0 then
        baseAddr <- baseAddr + yToggle

      for i in 0 .. 239 do
        let mutable tileAddr = baseAddr + ((xScroll &&& 0xf8) >>> 3)

        if (xScroll &&& xMask) <> 0 then
          tileAddr <- tileAddr + xToggle

        let tile = LinearTile.fromPrimitive (readVram16 reader tileAddr)
        let mutable x = xScroll &&& 7
        let mutable y = yScroll &&& 7

        xScroll <- xScroll + 1

        if tile.xFlip then x <- x ^^^ 7
        if tile.yFlip then y <- y ^^^ 7

        if bg.depth then
          let address = bg.colorBase + (tile.name <<< 6) + (y <<< 3) + (x >>> 0)

          outputPixel8Bpp reader bg i address
        else
          let address = bg.colorBase + (tile.name <<< 5) + (y <<< 2) + (x >>> 1)
          let palette = tile.palette <<< 4

          outputPixel4Bpp reader bg i address palette x


  let outputPixel15Bpp bg i address =
    bg.pixels.[i].opaque <- true
    bg.pixels.[i].colorAddress <- Constants.VramBase + address * 2


  let getPixel bg x priority (target1: IPixelSource ref) (target2: IPixelSource ref) =
    if bg.enable && bg.priority = priority && bg.pixels.[x].opaque then
      target2 := !target1
      target1 := bg :> IPixelSource


  let wireUp bg mmio =
    let readerStub _ = 0uy

    mmio (0x008 + (bg.index * 2)) (readControl0 bg) (writeControl0 bg)
    mmio (0x009 + (bg.index * 2)) (readControl1 bg) (writeControl1 bg)
    mmio (0x010 + (bg.index * 4)) readerStub (writeScrollX bg)
    mmio (0x011 + (bg.index * 4)) readerStub (writeScrollX bg)
    mmio (0x012 + (bg.index * 4)) readerStub (writeScrollY bg)
    mmio (0x013 + (bg.index * 4)) readerStub (writeScrollY bg)
