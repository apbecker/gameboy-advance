namespace GameBoyAdvance.Platform.Input

type InputBackend (index: int, numberOfButtons: int) =

  member __.Map (index: int) (button: string) = ()
  member __.Pressed(index: int) = false
  member __.Update () = ()
