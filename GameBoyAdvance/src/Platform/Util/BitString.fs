[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.BitString

let mask pattern =
  let folder value char =
    match char with
    | ' ' -> (value <<< 0) ||| 0
    | '0' -> (value <<< 1) ||| 1
    | '1' -> (value <<< 1) ||| 1
    |  _  -> (value <<< 1) ||| 0
  in
  String.fold folder 0 pattern


let test pattern =
  let folder value char =
    match char with
    | ' ' -> (value <<< 0) ||| 0
    | '0' -> (value <<< 1) ||| 0
    | '1' -> (value <<< 1) ||| 1
    |  _  -> (value <<< 1) ||| 0
  in
  String.fold folder 0 pattern
  

let bitMap decoder values pattern value =
  let mask =
    mask pattern
      |> decoder

  let test =
    test pattern
      |> decoder
  
  let size = Array.length values in

  for i in 0..(size - 1) do
    if (i &&& mask) = test then
      Array.set values i value
