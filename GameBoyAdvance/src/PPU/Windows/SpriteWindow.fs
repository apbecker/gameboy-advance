namespace GameBoyAdvance.PPU.Windows

type SpriteWindow =
  { buffer: bool[]
    mutable enabled: bool
    mutable insideFlags: WindowFlags }

module SpriteWindow =

  let init () =
    { buffer = Array.zeroCreate 240
      enabled = false
      insideFlags = WindowFlags.None }


  let inRange x y window =
    window.enabled && window.buffer.[x]


  let pixel x window =
    window.buffer.[x] <- true


  let pixelReset window =
    for i in 0 .. 239 do
      window.buffer.[i] <- false
