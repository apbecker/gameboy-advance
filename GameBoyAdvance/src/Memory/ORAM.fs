module GameBoyAdvance.Memory.ORAM

open GameBoyAdvance
open GameBoyAdvance.Platform
open GameBoyAdvance.Platform.Util


let init () =
  Memory.ram (BinaryUnit.kib 1)


let read size address memory =
  match size with
  | Size.Byte -> memory |> Memory.getUInt8  address |> int32
  | Size.Half -> memory |> Memory.getUInt16 address |> int32
  | Size.Word -> memory |> Memory.getUInt32 address |> int32


let write size address data memory =
  match size with
  | Size.Byte
  | Size.Half -> memory |> Memory.setUInt16 address (uint16 data)
  | Size.Word -> memory |> Memory.setUInt32 address (uint32 data)
