module GameBoyAdvance.Processors.ARM7.Vector

let [<Literal>] RST = 0x00
let [<Literal>] UND = 0x04
let [<Literal>] SWI = 0x08
let [<Literal>] PAB = 0x0c
let [<Literal>] DAB = 0x10
let [<Literal>] RES = 0x14
let [<Literal>] IRQ = 0x18
let [<Literal>] FIQ = 0x1c
