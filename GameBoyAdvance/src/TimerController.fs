namespace GameBoyAdvance

open GameBoyAdvance.CPU
open GameBoyAdvance.Memory

type TimerController =
  { timers: Timer []
    mutable counter: int }

module TimerController =

  let private shiftLut = [| 0; 6; 8; 10 |]


  let init () =
    { timers = [|
        Timer.init Interrupt.Timer0
        Timer.init Interrupt.Timer1
        Timer.init Interrupt.Timer2
        Timer.init Interrupt.Timer3
      |]
      counter = 0 }


  let getTimerIndex (address: int) ctrl =
    ctrl.timers.[(address >>> 2) &&& 3]


  let private readCounter0 ctrl = IOReader (fun address ->
    let index = ctrl |> getTimerIndex(address)

    uint8 (index.counter)
  )


  let private readCounter1 ctrl = IOReader (fun address ->
    let index = ctrl |> getTimerIndex(address)

    uint8 (index.counter >>> 8)
  )


  let private readControl0 ctrl = IOReader (fun address ->
    let index = ctrl |> getTimerIndex(address)

    uint8 (index.control)
  )


  let private readControl1 ctrl = IOReader (fun address ->
    let index = ctrl |> getTimerIndex(address)

    uint8 (index.control >>> 8)
  )


  let private writeCounter0 ctrl = IOWriter (fun address value ->
    let index = ctrl |> getTimerIndex(address)

    index.refresh <- index.refresh &&& 0xff00
    index.refresh <- index.refresh ||| (int value)
  )


  let private writeCounter1 ctrl = IOWriter (fun address value ->
    let index = ctrl |> getTimerIndex(address)

    index.refresh <- index.refresh &&& 0x00ff
    index.refresh <- index.refresh ||| ((int value) <<< 8)
  )


  let private writeControl0 ctrl = IOWriter (fun address value ->
    let index = ctrl |> getTimerIndex(address)

    if (index.control &&& 0x80) < ((int value) &&& 0x80) then
      index.counter <- index.refresh

    index.control <- index.control &&& 0xff00
    index.control <- index.control ||| (int value)
  )


  let private writeControl1 ctrl = IOWriter (fun address value ->
    let index = ctrl |> getTimerIndex(address)

    index.control <- index.control &&& 0x00ff
    index.control <- index.control ||| ((int value) <<< 8)
  )


  let wireUp mmio ctrl =
    mmio |> MMIO.rw 0x100 (readCounter0 ctrl) (writeCounter0 ctrl)
    mmio |> MMIO.rw 0x101 (readCounter1 ctrl) (writeCounter1 ctrl)
    mmio |> MMIO.rw 0x102 (readControl0 ctrl) (writeControl0 ctrl)
    mmio |> MMIO.rw 0x103 (readControl1 ctrl) (writeControl1 ctrl)

    mmio |> MMIO.rw 0x104 (readCounter0 ctrl) (writeCounter0 ctrl)
    mmio |> MMIO.rw 0x105 (readCounter1 ctrl) (writeCounter1 ctrl)
    mmio |> MMIO.rw 0x106 (readControl0 ctrl) (writeControl0 ctrl)
    mmio |> MMIO.rw 0x107 (readControl1 ctrl) (writeControl1 ctrl)

    mmio |> MMIO.rw 0x108 (readCounter0 ctrl) (writeCounter0 ctrl)
    mmio |> MMIO.rw 0x109 (readCounter1 ctrl) (writeCounter1 ctrl)
    mmio |> MMIO.rw 0x10a (readControl0 ctrl) (writeControl0 ctrl)
    mmio |> MMIO.rw 0x10b (readControl1 ctrl) (writeControl1 ctrl)

    mmio |> MMIO.rw 0x10c (readCounter0 ctrl) (writeCounter0 ctrl)
    mmio |> MMIO.rw 0x10d (readCounter1 ctrl) (writeCounter1 ctrl)
    mmio |> MMIO.rw 0x10e (readControl0 ctrl) (writeControl0 ctrl)
    mmio |> MMIO.rw 0x10f (readControl1 ctrl) (writeControl1 ctrl)


  let rec timerCarry sendIRQ elapsed n ctrl =
    elapsed n

    let timer = ctrl.timers.[n]
    if (timer.control &&& 0x40) <> 0 then
      sendIRQ timer.interrupt

    if n < 3 && (ctrl.timers.[n + 1].control &&& 0x84) = 0x84 then
      ctrl |> timerClock sendIRQ elapsed (n + 1) 1


  and timerClock sendIRQ elapsed n times ctrl =
    let timer = ctrl.timers.[n]

    let mutable counter = (timer.counter + times) &&& 0xffff
    if counter < timer.counter then
      counter <- counter + timer.refresh

      ctrl |> timerCarry sendIRQ elapsed n

    timer.counter <- counter


  let clock sendIRQ elapsed amount ctrl =
    let prev = ctrl.counter
    let next = (ctrl.counter + amount) &&& 0xffffff

    for i in 0 .. 3 do
      if (ctrl.timers.[i].control &&& 0x84) = 0x80 then
        let shift = shiftLut.[ctrl.timers.[i].control &&& 3]
        let tprev = prev >>> shift
        let tnext = next >>> shift

        let delta = tnext - tprev
        if delta > 0 then
          ctrl |> timerClock sendIRQ elapsed i delta

    ctrl.counter <- next
