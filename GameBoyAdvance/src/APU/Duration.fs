namespace GameBoyAdvance.APU

type Duration =
  { mask: int
    size: int
    mutable enabled: int
    mutable counter: int
    mutable counterLatch: int }

module Duration =

  let init size =
    { mask = size - 1
      size = size
      enabled = 0
      counter = 0
      counterLatch = 0 }


  let clock duration =
    if duration.enabled = 0 || duration.counter = 0 then
      false
    else
      duration.counter <- duration.counter - 1
      duration.counter = 0


  let reset duration =
    duration.counter <- duration.size - duration.counterLatch


  let write1 (data: byte) duration =
    duration.counterLatch <- ((int data) &&& duration.mask)


  let write2 (data: byte) duration =
    duration.enabled <- ((int data) >>> 6) &&& 1
