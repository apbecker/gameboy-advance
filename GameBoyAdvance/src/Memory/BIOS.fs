namespace GameBoyAdvance.Memory

open GameBoyAdvance
open GameBoyAdvance.CPU
open GameBoyAdvance.Platform

type BIOS =
  { memory: Memory
    mutable openBus: int }

module BIOS =

  let init (binary: byte[]) =
    { memory = Memory.rom(binary)
      openBus = 0 }


  let private readInternal size address bios =
    match size with
    | Size.Byte -> bios.memory |> Memory.getUInt8  address |> int32
    | Size.Half -> bios.memory |> Memory.getUInt16 address |> int32
    | Size.Word -> bios.memory |> Memory.getUInt32 address |> int32


  let read (cpu: Cpu) size address bios =
    if Cpu.getProgramCursor cpu < 0x4000 && address < 0x4000 then
      bios.openBus <- bios |> readInternal size address

    bios.openBus


  let write (_size: Size) (_address: int) (_data: int) (_bios: BIOS) =
    ()
