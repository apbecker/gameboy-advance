namespace GameBoyAdvance.PPU.Windows

open System

[<Flags>]
type WindowFlags =
  | None = 0x00
  | Bg0  = 0x01
  | Bg1  = 0x02
  | Bg2  = 0x04
  | Bg3  = 0x08
  | Obj  = 0x10
  | Clr  = 0x20
  | All  = 0x3f
