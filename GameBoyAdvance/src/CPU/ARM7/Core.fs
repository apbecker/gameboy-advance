namespace GameBoyAdvance.Processors.ARM7

open GameBoyAdvance
open GameBoyAdvance.Platform.Util

type ReadFunction = Size -> int -> int

type WriteFunction = Size -> int -> int -> unit

type Core =
  { cpsr: Flags
    mutable spsr: Option<Flags>
    spsrAbt: Flags
    spsrFiq: Flags
    spsrIrq: Flags
    spsrSvc: Flags
    spsrUnd: Flags
    pipeline: Pipeline
    mutable sp: Register
    mutable lr: Register
    mutable pc: Register
    registersAbt: Register[]
    registersFiq: Register[]
    registersIrq: Register[]
    registersSvc: Register[]
    registersUnd: Register[]
    registersUsr: Register[]
    registers: Register[]
    mutable code: int

    mutable carry: int
    mutable cycles: int
    mutable halt: bool
    mutable interrupt: bool }

  static member RegisterBank (size: int) =
    ArrayHelper.create1 (fun _ -> Register.init ()) size


  static member Create () =
    let registers = Core.RegisterBank 16

    let core =
      { registersAbt = Core.RegisterBank 2
        registersFiq = Core.RegisterBank 7
        registersIrq = Core.RegisterBank 2
        registersSvc = Core.RegisterBank 2
        registersUnd = Core.RegisterBank 2
        registersUsr = Core.RegisterBank 7
        registers    = registers
        cpsr = Flags.init ()
        spsr = None
        spsrAbt = Flags.init ()
        spsrFiq = Flags.init ()
        spsrIrq = Flags.init ()
        spsrSvc = Flags.init ()
        spsrUnd = Flags.init ()
        pipeline = Pipeline.init ()
        sp = registers.[13]
        lr = registers.[14]
        pc = registers.[15]
        code = 0

        carry = 0
        cycles = 0
        halt = false
        interrupt = false }

    core.Isr Mode.SVC Vector.RST
    core


  member this.ChangeRegisters (mode: int) =
    let mutable smallBank: Register[] = null
    let largeBank =
      if mode = Mode.FIQ then
        this.registersFiq
      else
        this.registersUsr

    this.registers.[ 8] <- largeBank.[6]
    this.registers.[ 9] <- largeBank.[5]
    this.registers.[10] <- largeBank.[4]
    this.registers.[11] <- largeBank.[3]
    this.registers.[12] <- largeBank.[2]

    match mode with
    | Mode.ABT -> smallBank <- this.registersAbt; this.spsr <- Some this.spsrAbt
    | Mode.FIQ -> smallBank <- this.registersFiq; this.spsr <- Some this.spsrFiq
    | Mode.IRQ -> smallBank <- this.registersIrq; this.spsr <- Some this.spsrIrq
    | Mode.SVC -> smallBank <- this.registersSvc; this.spsr <- Some this.spsrSvc
    | Mode.SYS -> smallBank <- this.registersUsr; this.spsr <- None
    | Mode.UND -> smallBank <- this.registersUnd; this.spsr <- Some this.spsrUnd
    | Mode.USR -> smallBank <- this.registersUsr; this.spsr <- None
    | _ -> ()

    this.registers.[13] <- smallBank.[1]
    this.registers.[14] <- smallBank.[0]

    this.sp <- this.registers.[13]
    this.lr <- this.registers.[14]
    this.pc <- this.registers.[15]


  member this.GetCondition (condition: int) =
    match condition &&& 15 with
    | 0x0 -> (* EQ *) this.cpsr.z <> 0
    | 0x1 -> (* NE *) this.cpsr.z = 0
    | 0x2 -> (* CS *) this.cpsr.c <> 0
    | 0x3 -> (* CC *) this.cpsr.c = 0
    | 0x4 -> (* MI *) this.cpsr.n <> 0
    | 0x5 -> (* PL *) this.cpsr.n = 0
    | 0x6 -> (* VS *) this.cpsr.v <> 0
    | 0x7 -> (* VC *) this.cpsr.v = 0
    | 0x8 -> (* HI *) this.cpsr.c <> 0 && this.cpsr.z = 0
    | 0x9 -> (* LS *) this.cpsr.c = 0 || this.cpsr.z <> 0
    | 0xa -> (* GE *) this.cpsr.n = this.cpsr.v
    | 0xb -> (* LT *) this.cpsr.n <> this.cpsr.v
    | 0xc -> (* GT *) this.cpsr.n = this.cpsr.v && this.cpsr.z = 0
    | 0xd -> (* LE *) this.cpsr.n <> this.cpsr.v || this.cpsr.z <> 0
    | 0xe -> (* AL *) true
    | _   -> (* NV *) false


  member this.Isr mode vector =
    this.ChangeRegisters(mode)

    this.spsr
      |> Option.iter (Flags.copy this.cpsr)

    this.lr.value <- if this.cpsr.t = 1 then this.pc.value - 2 else this.pc.value - 4
    this.pc.value <- vector
    this.pipeline.refresh <- true

    if vector = Vector.FIQ || vector = Vector.RST then
      this.cpsr.f <- 1

    this.cpsr.t <- 0
    this.cpsr.i <- 1
    this.cpsr.m <- mode


  member this.MoveSPSRToCPSR () =
    this.spsr
      |> Option.iter (fun spsr -> this.cpsr |> Flags.copy spsr)

    this.ChangeRegisters(this.cpsr.m)


  member this.GetProgramCursor () =
    this.pc.value


  member this.Get (index: int) r15Offset =
    if index = 15 then
      this.registers.[index].value + r15Offset
    else
      this.registers.[index].value


  member this.Set (index: int) (value: int) =
    if index = 15 then
      this.registers.[index].value <- value
      this.pipeline.refresh <- true
    else
      this.registers.[index].value <- value


  member this.ReadWord reader (address: int) =
    let value = this.Read reader Size.Word (address &&& (~~~3))
    let shift = (address &&& 3) * 8

    (this.USR value shift) ||| (value <<< (32 - shift))


  member this.ReadHalf reader address =
    let value = this.Read reader Size.Half (address &&& (~~~1)) |> uint16 |> int32
    let shift = (address &&& 1) * 8

    (value >>> shift) ||| (value <<< (32 - shift))


  member this.ReadHalfSignExtended reader address =
    let value = this.Read reader Size.Half (address &&& (~~~1)) |> int16 |> int32
    let shift = (address &&& 1) * 8

    (value >>> shift)


  member this.ReadByte reader address =
    this.Read reader Size.Byte address
      |> uint8
      |> int32


  member this.ReadByteSignExtended reader address =
    this.Read reader Size.Byte address
      |> int8
      |> int32


  member __.Read reader size address =
    reader size address


  member __.Write writer size address data =
    writer size address data


  static member MultiplierCycles value =
    match value with
    | n when ((n &&& 0xffffff00) = 0 || (n &&& 0xffffff00) = 0xffffff00) -> 1
    | n when ((n &&& 0xffff0000) = 0 || (n &&& 0xffff0000) = 0xffff0000) -> 2
    | n when ((n &&& 0xff000000) = 0 || (n &&& 0xff000000) = 0xff000000) -> 3
    | _ -> 4


  static member GetBit (n: int) (value: int) =
    (value >>> n) &&& 1


  member this.AddCarry (a: int) (b: int) carry =
    let r = (a + b + carry)

    if this.cpsr.t <> 0 || (this.code &&& (1 <<< 20)) <> 0 then
      let overflow = ~~~(a ^^^ b) &&& (a ^^^ r)

      this.cpsr.n <- Core.GetBit 31 r
      this.cpsr.z <- if r = 0 then 1 else 0
      this.cpsr.c <- Core.GetBit 31 (overflow ^^^ a ^^^ b ^^^ r)
      this.cpsr.v <- Core.GetBit 31 (overflow)

    r


  member this.Add a b =
    this.AddCarry a b 0


  member this.Sub a b =
    this.AddCarry a (~~~b) 1


  member this.SubCarry a b carry =
    this.AddCarry a (~~~b) carry


  member this.UMul (a: int) (b: int) =
    let ua = uint32 a
    let ub = uint32 b

    int32 (ua * ub)


  member this.Mul (a: int) (b: int) (c: int) =
    this.cycles <- this.cycles + (Core.MultiplierCycles b)

    let result = (this.UMul a b) + c

    if this.cpsr.t <> 0 || (this.code &&& (1 <<< 20)) <> 0 then
      this.cpsr.n <- Core.GetBit 31 result
      this.cpsr.z <- if result = 0 then 1 else 0

    result


  member this.Mov (value: int) =
    if this.cpsr.t <> 0 || (this.code &&& (1 <<< 20)) <> 0 then
      this.cpsr.n <- Core.GetBit 31 value
      this.cpsr.z <- if value = 0 then 1 else 0
      this.cpsr.c <- this.carry

    value


  member this.LSL (value: int) (shift: int) =
    if shift = 0 then
      this.carry <- this.cpsr.c
      value
    else
      this.carry <- if shift > 32 then 0 else Core.GetBit (32 - shift) value
      if (shift > 31) then 0 else (value <<< shift)


  member this.USR (value: int) (shift: int) =
    ((uint32 value) >>> shift)
      |> int32


  member this.LSR (value: int) (shift: int) =
    if shift = 0 then
      this.carry <- this.cpsr.c
      value
    else
      this.carry <- if shift > 32 then 0 else Core.GetBit (shift - 1) value
      if shift > 31 then 0 else this.USR value shift


  member this.ASR (value: int) (shift: int) =
    if shift = 0 then
      this.carry <- this.cpsr.c
      value
    else
      let carryBit = if shift > 32 then 31 else shift - 1
      this.carry <- Core.GetBit carryBit value

      if shift > 31 then (value >>> 31) else (value >>> shift)


  member this.ROR (value: int) (shift: int) =
    if shift = 0 then
      this.carry <- this.cpsr.c
      value
    else
      let shift = shift &&& 31

      this.carry <- (this.USR value (shift - 1)) &&& 1
      (this.USR value shift) ||| (value <<< (32 - shift))


  member this.RRX (value: int) =
    this.carry <- value &&& 1
    (this.USR value 1) ||| (this.cpsr.c <<< 31)
