[<RequireQualifiedAccess>]
module GameBoyAdvance.Platform.Util.ColorHelper

let fromRGB (r: byte) (g: byte) (b: byte) =
  let a = 255uy in

  ((int a) <<< 24) |||
  ((int r) <<< 16) |||
  ((int g) <<<  8) |||
  ((int b) <<<  0)
